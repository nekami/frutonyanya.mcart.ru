<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?
if (CModule::IncludeModule('main'))
{
	$result = \Bitrix\Main\UserTable::getList(array(
		'select' => array('ID'),
		'filter' => ['ACTIVE' => 'Y', '=UF_CONFIRMATION_EMAIL' => false],
	));
	
	$user = new CUser;	
		
	while ($arUser = $result->fetch()) 
	{
		$user->Update($arUser["ID"], ["UF_CONFIRMATION_EMAIL" => "Y"]);	
	}		
}

?>