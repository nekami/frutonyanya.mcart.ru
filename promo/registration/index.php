<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	"",
	Array(
		"AUTH" => "Y",
		"REQUIRED_FIELDS" => array("EMAIL","NAME"),
		"SEF_FOLDER" => "/",
		"SEF_MODE" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL","NAME","PERSONAL_PHONE","PERSONAL_CITY"),
		"SUCCESS_PAGE" => "",
		"USER_PROPERTY" => array(),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
		"VARIABLE_ALIASES" => Array()
	)
);?> 

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>