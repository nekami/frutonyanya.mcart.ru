<?
if ( !empty($_GET['confirm_registration']) && !empty($_GET['confirm_user_id']) )
{
	if (CModule::IncludeModule('main'))
	{
		$idUser = trim($_GET['confirm_user_id']);		
		$user = new CUser;		
		$user->Update($idUser, ["UF_CONFIRMATION_EMAIL" => "Y"]);
	}
}

addEventHandler("main", "OnAfterUserAdd", "AfterUserAdd");
 
function AfterUserAdd(&$arFields)
{
    if ( $arFields['EXTERNAL_AUTH_ID'] == 'socservices' )
	{
		CModule::IncludeModule("main");
		
		CUser::SetUserGroup($arFields['ID'], array_merge(CUser::GetUserGroup($arFields['ID']), [GROUP_CONSULTANTS]));
	}
}

function getCaptcha($SecretKey) 
{
	$Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response={$SecretKey}");
	$Return = json_decode($Response);
	return $Return;
}

function s($var, $userID = 0)
{
    global $USER;
    
    if (($userID > 0 && $USER->GetID() == $userID) || $userID == 0) {
        if ($var === false) {
            $var = "false";
        }
        echo "<xmp>";
        print_r($var);
        echo "</xmp>";
    }
}

function p($var, $url = false, $delimiter = false)
{
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}

function isUserPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();

    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);

    return ($password == $realPassword);
}

function GetDocument($Doc)
{	
	CModule::IncludeModule('iblock');
	
	$phpCache = new CPHPCache();
			
	if ($phpCache->InitCache(360000, $Doc, '/promo_documents')) 
	{
		$data = $phpCache->GetVars();    
		$Document = $data['Document'];
	} 
	else 
	{
		$Document = "";

		$obDocument = CIBlockElement::GetList([], ["IBLOCK_CODE" => "documentation", "CODE" => $Doc], false, false, ["ID", "CODE", "PROPERTY_FILE"]);

		if ($arDocument = $obDocument->fetch())
			$Document = CFile::GetPath($arDocument["PROPERTY_FILE_VALUE"]);
			
		$phpCache->StartDataCache();
		$phpCache->EndDataCache(['Document' => $Document]);
	}
	
	return $Document;
}

function GetWinners()
{
	if (CModule::IncludeModule('main'))
	{
		$result = \Bitrix\Main\UserTable::getList(array(
			'select' => array('ID', 'NAME', 'EMAIL'),
			'filter' => ['ACTIVE' => 'Y', '!=UF_WINNER' => false, "=UF_NOTIFICATION_VICTORY" => false],
		));
		
		while ($arUser = $result->fetch()) 
		{
			if ( !empty($arUser['ID']) )
			{
				$user = new CUser;		
				$user->Update($arUser['ID'], ["UF_NOTIFICATION_VICTORY" => "Y"]);
				
				CEvent::Send("WINNER_PROMO", ["s1", "s2"], ["USER_ID" => $arUser['ID'], "EMAIL" => $arUser['EMAIL'], "NAME" => $arUser['NAME']], "N", 100);
			}
				
		}		
	}

	return "GetWinners();";
}
?>