<?
include_once(__DIR__."/include/functions.php");

include_once(__DIR__."/include/const.php");

set_time_limit(14400);
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

Main\Loader::includeModule('tagency.frutonyanya'); # Основной модуль проекта


##################### Dependencies #####################
require_once(__DIR__ . '/bootstrap.php');

//start refactoring TODO remove after refatoring complete
session_start();
define("HLB_ID_ART_AGES", 5); // ID highload блока с возрастом для статей
define("HLB_ID_ART_TAGS", 4); // ID highload блока с тегами для статей
define("HLB_ID_ART_VIEWS", 6); // ID highload блока с просмотрами статей

//recaptcha keys
define('RECAPTCHA_SITE_KEY', '6LdG8qwUAAAAAJvVyF1U4m5imvr9038Q4KKQkwrk');
define('RECAPTCHA_SECRET_KEY', '6LdG8qwUAAAAACytugo0rxwVXWLk49wKePS32Oqu');

//yandex keys
define('YANDEX_MAP_API_KEY', "e2410605-341c-4487-a36c-5b766be07c79");
define('YANDEX_AFISHA_TOKEN', "TYtfglV7g24qHcguiIYgiNSFeNSSRaNK");
define('YANDEX_AFISHA_KEY', "b9d40aae-34fe-49db-80cd-bce73b7141d1");
define('YANDEX_SPRAVOCHNIK_KEY', "27091afc-b022-4374-b5ca-458c8b506658");

define('EVENTS_IBLOCK', 27); //инфобок событий
define('SESSIONS_IBLOCK', 28); //инфоблок сеансов
define('EVENT_IMPORT_CHUNK', 50);  //размер чанка импорта событий
define('EVENT_IMAGES_IMPORT_CHUNK', 5000);  //размер чанка импорта изображений

define('EVENTS_COUNT', 50); //количество событий в городе, при котором выводятся подборки

define('HH_API_EMAIL', 'mirex.doors@mail.ru'); //email для работы с апи хэдхантера (требуется в заголовке)

define('CRM_API_URL', "https://frutonyanyacrm.traf.spb.ru/api/v1/"); // url где лежат все чеки
define('CRM_API_TOKEN', 'Qm%fEX"(Hx2f-Sq8Mh4a%bbU<"}g+5k<cYv9\C75Y!@bb\DK?\'h2%]z/^jfv'); // token для доступа к crm чекам


\Bitrix\Main\Loader::includeModule('sale');

$classPath = '/local/php_interface/classes';
Main\Loader::registerAutoLoadClasses(null, array(
    '\Traffic\Helpers' => $classPath . '/helpers.php',
    '\Traffic\Users\AuthFactory' => $classPath . '/users/authFactory.php',
    '\Traffic\Users\Auth' => $classPath . '/users/auth.php',
    '\Traffic\Users\FB' => $classPath . '/users/fb.php',
    '\Traffic\Users\VK' => $classPath . '/users/vk.php',
    '\Traffic\Users\OK' => $classPath . '/users/ok.php',
    '\Traffic\Users\User' => $classPath . '/users/user.php',
    '\Traffic\Bonus\Lib\Crm' => $classPath . '/bonus/lib/crm.php',
    '\Traffic\Bonus\Lib\User' => $classPath . '/bonus/lib/user.php',
    '\Traffic\Bonus\Lib\CheckCRM' => $classPath . '/bonus/lib/checkcrm.php',
    '\Traffic\Bonus\Coin' => $classPath . '/bonus/coin.php',
    '\MdOption\Entities\UserOptionBlockKidsTable' => $classPath . '/entities/UserOptionBlockKidsTable.php',
    '\MdOption\UserOptionBlockKids' => $classPath . '/UserOptionBlockKids.php',
));
//агент удаления фото
function deleteImagesAgent()
{
    $imagesDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/images/';
    deleteTree($imagesDir);
    return "deleteImagesAgent();";
}

//цвет ветки станции метро
function getMetroColor($cityName, $stationNames)
{
    $resultStations = false;
    $cityId = false;

    $areas = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/json/metro.json');
    $areas = json_decode($areas, 1);

    foreach ($areas[0]['areas'] as $subject) {
        if ($subject['name'] == $cityName) {
            $cityId = $subject['id'];
            break;
        }
        foreach ($subject['areas'] as $city) {

            if ($city['name'] == $cityName) {
                $cityId = $city['id'];
                break;
            }
        }

    }

    if ($cityId) {
        $resultStations = getColor($cityId, $stationNames);
    }
    return $resultStations;
}


function getColor($city, $currentStations)
{

    $resultStations = [];
    $url = "https://api.hh.ru/metro/" . $city;

    $curl = curl_init();
    $headers = ["Content-Type: application/json", "User-Agent: Frutonyanya/1.0 (" . HH_API_EMAIL . ")"];
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
    ));
    $result = curl_exec($curl);
    curl_close($curl);

    if ($result) {
        $result = json_decode($result, 1);
        foreach ($result['lines'] as $lineKey => $line) {

            foreach ($line['stations'] as $stationKey => $station) {

                if (in_array($station['name'], $currentStations)) {
                    $resultStation['name'] = $station['name'];
                    $resultStation['hex_color'] = $line['hex_color'];
                    $resultStations[] = $resultStation;
                }
            }
        }

    }
    return $resultStations;
}

function getYandexUserCityId($userId)
{
    $rsUser = CUser::GetByID($userId);
    $arUser = $rsUser->Fetch();
    $cityRus = $arUser['PERSONAL_CITY'];


    $res = \Bitrix\Sale\Location\LocationTable::getList([
        'filter' => ['NAME.NAME' => $cityRus],
        'select' => ["ID", "NAME.NAME"]
    ]);
    $loc = $res->fetch();
    $cityId = $loc['ID'];


    $arLocs = CSaleLocation::GetByID($cityId, "ru");
    $cityName = $arLocs['CITY_NAME'];
    return getCityIdByName($cityName);
}

function getPregnancyWeek($userId)
{
    //Проверяем наличие детей
    $arSelect = array("ID", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_BIRTHDAY", "NAME");
    $result = array();
    $arFilter = array("IBLOCK_ID" => 18, "ACTIVE" => "Y", "=PROPERTY_USER" => $userId);
    $res = CIBlockElement::GetList(
        array("PROPERTY_BIRTHDAY" => "DESC"),
        $arFilter,
        false,
        false,
        $arSelect
    );
    while ($ob = $res->GetNext()) {
        // получаем количество недель до рождения
        $birthday = strtotime($ob["PROPERTY_BIRTHDAY_VALUE"]);
        $now = strtotime("now");
        $dateDiff = 42 - ($birthday - $now) / (60 * 60 * 24 * 7); // недель
        if ($dateDiff > 0) {
            $result[] = round($dateDiff);
        }
    }
    return $result;
}

function getCityIdByName($name)
{
    $yandexCityId = "N";
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK, "NAME" => $name);
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array("UF_CITYID"));
    if ($arSection = $rsSections->GetNext()) {
        $yandexCityId = $arSection['UF_CITYID'];
    }
    return $yandexCityId;
}

function getSessionsById($cityId, $eventId)
{
    $result = [];
    $sessionIds = [];
    $sessions = json_decode(
        file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/sessions.json'),
        1
    );
    $i = 0;
    $venues = json_decode(
        file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/venues.json'),
        1
    );

    foreach ($sessions as $session) {
        if ($session['event_id'] == $eventId && $session['sale']['available']) {
            $el = new CIBlockElement;
            $datetime = explode("T", $session['datetime']);
            $PROP = array();
            $PROP['DATE'] = $datetime[0]; //дата

            if ($datetime[1] != "00:00:00") {
                $PROP['TIME'] = $datetime[1];  //время
            }

            foreach ($venues as $venue) {
                if ($venue['id'] == $session['venue_id']) {
                    $PROP['VENUE'] = $venue['title'];
                    break;
                }
            }

            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => SESSIONS_IBLOCK,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => $session['sale']['id'],
                "ACTIVE" => "Y",
            );

            $result[] = $el->Add($arLoadProductArray);
            $i++;
            if ($i == 4) {
                break;
            }
        }
    }
    return $result;
}

function getVenueById($cityId, $eventId)
{
    $venueId = false;
    $resultVenue = [];
    $sessions = json_decode(
        file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/sessions.json'),
        1
    );
    foreach ($sessions as $session) {
        if ($session['event_id'] == $eventId) {
            $venueId = $session['venue_id'];
            break;
        }
    }

    if ($venueId) {
        $venues = json_decode(
            file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/venues.json'),
            1
        );
        foreach ($venues as $venue) {
            if ($venue['id'] == $venueId) {
                $resultVenue['title'] = $venue['title'];
                $resultVenue['address'] = $venue['address'];
                $resultVenue['link'] = $venue['links'][0];
                $resultVenue['metro'] = $venue['metro'];
                $resultVenue['location'] = $venue['location'];
                break;
            }
        }
    }
    return $resultVenue;
}


function deleteTree($dir)
{
    $files = glob($dir . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (substr($file, -1) == '/') {
            deleteTree($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dir);
}


function downloadEvents()
{
    $url = "https://afisha.yandex.ru/export/json";
    $TOKEN = "Authorization: Token " . YANDEX_AFISHA_TOKEN;
    $headers = [];
    $headers[] = $TOKEN;
    $zipUrl = "";
    $pathDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/';
    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/events/')) {
        mkdir($_SERVER['DOCUMENT_ROOT'] . '/upload/events/');
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
    ));
    $result = curl_exec($curl);
    curl_close($curl);

    if ($result) {
        $zipUrl = json_decode($result, 1)[0]['url'];  //ссылка на архив с событиями
        //сохраняем архив
        if ($zipUrl != "") {
            if (!realpath($pathDir)) {
                mkdir($pathDir, 0755);
            }
            $filename = 'afisha_file_' . date('Y-m-d') . '.tar.gz';
            $pathFile = $pathDir . $filename;
            file_put_contents($pathFile, file_get_contents($zipUrl));

            require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/tar_gz.php');
            $oArchiver = new CArchiver($pathFile);
            $tres = $oArchiver->extractFiles($pathDir);
            if ($tres) {
                unlink($pathDir . $filename);
//                unlink($pathFile);
            }
        }
        return $pathDir;
    }
}

function createImagesAgentShow()
{
    $i = 0;
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_IMAGES_LINKS");
    $arFilter = Array("IBLOCK_ID" => EVENTS_IBLOCK, "!PROPERTY_IMAGES_LINKS" => '{"image":null,"sub":[]}','PREVIEW_PICTURE'=>false);
    $elements = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => EVENT_IMAGES_IMPORT_CHUNK),
        $arSelect);
    while ($element = $elements->GetNext()) {
        $i++;
    }
    print_r($i);
    return 'createImagesAgentShow();';
}

function getResizeImage($url, $isMainImage)
{
    $arFile = CFile::MakeFileArray($url); //получаем данные о файле

    if ($arFile["type"] == "image/webp"){

        if (file_exists('./example.jpeg')){
            unlink('./example.jpeg');
        }

        $im = imagecreatefromwebp($url);
        imagejpeg($im, './example.jpeg', 100);
        imagedestroy($im);
        $arFile = CFile::MakeFileArray('./example.jpeg');
    }



    $arFile['name'] = $arFile['name'] . '.jpg';
    $arFile["del"] = true;
    $arFile["MODULE_ID"] = "iblock";
    $fileId = CFile::SaveFile($arFile, "afisha");
    if ($isMainImage) {
        $resizedFile = [];
        $resizedFile['DETAIL_PICTURE'] = CFile::MakeFileArray(CFile::ResizeImageGet($fileId,
            array('width' => 1000, 'height' => 350), BX_RESIZE_IMAGE_EXACT)['src']);
        $resizedFile['PREVIEW_PICTURE'] = CFile::MakeFileArray(CFile::ResizeImageGet($fileId,
            array('width' => 400, 'height' => 266), BX_RESIZE_IMAGE_EXACT)['src']);
    } else {
        $resizedFile = CFile::MakeFileArray(CFile::ResizeImageGet($fileId,
            array('width' => 360, 'height' => 240), BX_RESIZE_IMAGE_EXACT)['src']);
    }
	//CFile::Delete($fileId);
    return $resizedFile;
}

function getSelections($cityId, $selectionsId)
{
    $resultSelections = array();
    $selectionsJSON = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/selections.json';
    $selections = json_decode(file_get_contents($selectionsJSON), 1);
    foreach ($selections as $selection) {
        if (in_array($selection['id'], $selectionsId) && !in_array($selection['title'], $resultSelections)) {
            $resultSelections[] = $selection['title'];
        }
    }
    return $resultSelections;
}

function getActors($persons)
{
    $result = [];
    foreach ($persons as $person) {
        if ($person['role'] == 'actor') {
            $result[] = $person['name'];
        }
    }
    return $result;
}

function getDirector($persons)
{
    $result = "";
    foreach ($persons as $person) {
        if ($person['role'] == 'director') {
            $result = $person['name'];
            break;
        }
    }
    return $result;
}


function createImagesAgent()
{
    $i = 0;
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_IMAGES_LINKS");
	//$arFilter = Array("IBLOCK_ID" => EVENTS_IBLOCK, "!PROPERTY_IMAGES_LINKS" => false,'PREVIEW_PICTURE'=>false);
    $arFilter = Array("IBLOCK_ID" => EVENTS_IBLOCK, "!PROPERTY_IMAGES_LINKS" => '{"image":null,"sub":[]}','PREVIEW_PICTURE'=>false);
    $elements = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 5000),
        $arSelect);
    while ($element = $elements->GetNext()) {
        $i++;

        $images = json_decode(htmlspecialcharsBack($element['PROPERTY_IMAGES_LINKS_VALUE']), 1);
        $resizedImage = getResizeImage($images['image'], true);


        $newElement = new CIBlockElement;
        $arLoadProductArray = Array(
            'DETAIL_PICTURE' => $resizedImage['DETAIL_PICTURE'],
            'PREVIEW_PICTURE' => $resizedImage['PREVIEW_PICTURE'],
        );
		$newElement->Update($element['ID'], $arLoadProductArray);

        $arFile = array();
        foreach ($images['sub'] as $k => $subImage) {
            $file = getResizeImage($subImage['origin_url'], false);
            $arFile[] = array("VALUE" => $file, "DESCRIPTION" => "");
        }
		CIBlockElement::SetPropertyValueCode($element['ID'], "MORE_IMAGES", $arFile);
        /*CIBlockElement::SetPropertyValueCode($element['ID'], "IMAGES_LINKS", "");*/
		//var_dump($element['ID']);
		//die();
    }
    return 'createImagesAgent();';
}

function createEvents($splice)
{
    CModule::IncludeModule("iblock");
    $params = Array(
        "max_len" => "100", // обрезает символьный код до 100 символов
        "change_case" => "L", // буквы преобразуются к нижнему регистру
        "replace_space" => "_", // меняем пробелы на нижнее подчеркивание
        "replace_other" => "_", // меняем левые символы на нижнее подчеркивание
        "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
        "use_google" => "false", // отключаем использование google
    );
    $i = 0;
    $break = false;
    /*перебираем разделы*/
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK);
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID', 'UF_CITYID'));
    while ($arSection = $rsSections->GetNext()) {
        $cityId = $arSection['UF_CITYID'];
        /*получаем файл с событиями*/
        $eventsJSON = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/events.json';
        $events = json_decode(file_get_contents($eventsJSON), 1);

        foreach ($events as $event) {
            //проверка на "детский" раздел
            if (in_array($event['id'], $splice)) {
                $arSelect = Array("ID", "IBLOCK_ID");
                $arFilter = Array(
                    "IBLOCK_ID" => EVENTS_IBLOCK,
                    "IBLOCK_SECTION_ID" => $arSection['ID'],
                    "PROPERTY_EVENT_ID" => $event['id']
                );
                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1), $arSelect);
                if (!$res->GetNext()) {
                    $PROP = array();
                    $PROP['EVENT_ID'] = $event['id'];  // ID события
                    $venue = getVenueById($cityId, $event['id']); //местоположение
                    $PROP['VENUE'] = $venue['title'];
                    $PROP['SESSION'] = getSessionsById($cityId, $event['id']); //id сеанса
                    $PROP['VENUE_LINK'] = $venue['link']; //Ссылка на место проведения
                    $PROP['VENUE_ADDRESS'] = $venue['address']; //Адрес места проведения
                    $PROP['VENUE_METRO'] = $venue['metro']; //Адрес места проведения
                    $PROP['VENUE_LOCATION'] = implode(",", $venue['location']); //Адрес места проведения
                    $PROP['EVENT_URL'] = $event['url']; //url в афише
                    $PROP['DATES'] = $event['dates']; //даты
                    $PROP['COID'] = $event['coid']; //id кинопоиска
                    $PROP['SELECTIONS'] = getSelections($cityId, $event['selections']); //подборки
                    $PROP['DURATION'] = $event['duration'];
                    $PROP['DIRECTOR'] = getDirector($event['persons']);
                    $PROP['YEAR'] = $event['year'];
                    $PROP['ACTORS'] = getActors($event['persons']);
                    $PROP['COUNTRY'] = implode(",", $event['countries']);

                    /*изображения*/
                    $images = [
                        "image" => $event['image']['origin_url'],
                        "sub" => array()
                    ];
                    $imagesCounter = 0;
                    if (!empty($event['image_gallery'])) {
                        foreach ($event['image_gallery'] as $image) {
                            if ($imagesCounter == 4) {
                                break;
                            }
                            if ($image['type'] == "image" && $image['sub_type'] != "other") {
                                $images['sub'][] = $image;
                                $imagesCounter++;
                            }
                        }
                    }

                    $PROP['IMAGES_LINKS'] = json_encode($images);
                    /*кинопоиск*/
                    if ($event['ratings']['kinopoisk'] > 0) {
                        $PROP['KINOPOISK_RATE'] = $event['ratings']['kinopoisk'];
                    }
                    /* цена*/
                    if (!empty($event['prices'])) {
                        foreach ($event['prices'] as $price) {
                            if ($price['currency_code'] == 'rub') {
                                $PROP['PRICE'] = substr($price['value'], 0, -2) . " ₽";
                                break;
                            }
                        }
                    }


                    $el = new CIBlockElement;
                    $description = strip_tags($event['description']);
                    $arLoadProductArray = Array(
                        "IBLOCK_SECTION_ID" => $arSection['ID'],
                        "IBLOCK_ID" => EVENTS_IBLOCK,
                        "NAME" => $event['title'],
                        "CODE" => CUtil::translit($event['title'], "ru", $params),
                        "PROPERTY_VALUES" => $PROP,
                        "ACTIVE" => "Y",
                        "PREVIEW_TEXT" => substr($description, 0, 50) . "...",
                        "DETAIL_TEXT" => $description
                    );

                    //Сохранение дополнительных изображений
                    $el->Add($arLoadProductArray);
                    $i++;
                    if ($i == count($splice)) {
                        $break = true;
                        break;

                    }
                }
            }
        }
        if ($break) {
            break;
        }
    }
}

function getRandomSelectionName($cityId, &$selections)
{
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK, "=UF_CITYID" => $cityId);
    $arSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'PREVIEW_PICTURE',
        'PREVIEW_TEXT',
        'DETAIL_PAGE_URL',
        'PROPERTY_VENUE',
        'PROPERTY_DATES',
        'PROPERTY_EVENT_ID'
    );
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID'));
    $arSection = $rsSections->GetNext();

    if (count($selections) > 0) {
        $randKey = array_rand($selections);
        $randomSelection = $selections[$randKey];

        $arSelect = Array("ID", "NAME", "IBLOCK_ID");
        $arFilter = Array(
            "IBLOCK_ID" => EVENTS_IBLOCK,
            "SECTION_ID" => $arSection['ID'],
            "PROPERTY_SELECTIONS" => $randomSelection
        );
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNext()) {
            return $randomSelection;
        } else {
            unset($selections[$randKey]);
            return getRandomSelectionName($cityId, $selections);
        }
    } else {
        return false;
    }
}

function getAllEvents()
{
    $result = array();
    /*перебираем разделы*/
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK);
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID', 'UF_CITYID'));
    while ($arSection = $rsSections->GetNext()) {
        $cityId = $arSection['UF_CITYID'];
        /*получаем файл с событиями*/
        $eventsJSON = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/events.json';
        $events = json_decode(file_get_contents($eventsJSON), 1);

        foreach ($events as $event) {
            //проверка на "детский" раздел
            if (in_array("kids", $event['tags']) || in_array("kids", $event['session_tags'])) {
                $result[] = $event['id'];
            }
        }
    }
    return $result;
}

function deleteOldEventsAgent()
{
    CModule::IncludeModule("iblock");
    $importJsonPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/events.json'; //файл с позициями импорта
    if (!file_exists($importJsonPath)) {
        downloadEvents(); // получаем новую выгрузку
        $allImportedEvents = getAllEvents(); //получаем все актиуальные события

        /*получаем все текущие события*/
        $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_EVENT_ID");
        $arFilter = Array("IBLOCK_ID" => EVENTS_IBLOCK);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNext()) {
            if (in_array($ob['PROPERTY_EVENT_ID_VALUE'], $allImportedEvents)) {
                CIBlockElement::Delete($ob['ID']);
            }
        }
        $eventsDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/';
        deleteTree($eventsDir);    //удаляем выгрузку
        unlink($importJsonPath);
    }
    return 'deleteOldEventsAgent();';
}

function deleteOldSessionsAgent()
{
    $currentDate = date('Y-m-d');
    $currentTimestamp = date('U');
    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_DATE", "PROPERTY_TIME");
    $arFilter = Array("IBLOCK_ID" => SESSIONS_IBLOCK, "<=PROPERTY_DATE" => $currentDate);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNext()) {
        if ($ob['PROPERTY_TIME_VALUE'] != '00:00:00') {  //если у события указано время, то проверяем метку
            if ($currentTimestamp > strtotime($ob['PROPERTY_DATE_VALUE'] . ' ' . $ob['PROPERTY_TIME_VALUE'])) {
                CIBlockElement::Delete($ob['ID']);
            }
        } else {
            CIBlockElement::Delete($ob['ID']);
        }
    }
    return 'deleteOldSessionsAgent();';
}

function eventsAgent()
{
    $importJsonPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/events.json'; //файл с позициями импорта
    if (!file_exists($importJsonPath)) {
        downloadEvents(); // получаем новую выгрузку
        \Tagency\Frutonyanya\Events::getInstance()->createSections(); //создаём разделы
        $allImportedEvents = getAllEvents(); //получаем все события
        $splicedEvents = array_slice($allImportedEvents, 0, EVENT_IMPORT_CHUNK);
        $allImportedEvents = array_splice($allImportedEvents, EVENT_IMPORT_CHUNK);
        \Tagency\Frutonyanya\Events::getInstance()->create($splicedEvents); //Импортируем чанк
        $resizeDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/resize_cache/afisha/';
        deleteTree($resizeDir);
        $imagesDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/afisha/';
        deleteTree($imagesDir);
        if (count($allImportedEvents) > 0) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events.json', json_encode($allImportedEvents));
        } else {
            unlink($importJsonPath);
        }
    } else {
        $allImportedEvents = json_decode(file_get_contents($importJsonPath), 1);
        $splicedEvents = array_slice($allImportedEvents, 0, EVENT_IMPORT_CHUNK);
        $allImportedEvents = array_splice($allImportedEvents, EVENT_IMPORT_CHUNK);
        \Tagency\Frutonyanya\Events::getInstance()->create($splicedEvents); //Импортируем чанк
        $resizeDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/resize_cache/afisha/';
        deleteTree($resizeDir);
        $imagesDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/afisha/';
        deleteTree($imagesDir);
        if (count($allImportedEvents) > 0) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/events.json', json_encode($allImportedEvents));
        } else {
            $eventsDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/';
            deleteTree($eventsDir);    //удаляем выгрузку
            unlink($importJsonPath);
        }
    }
    return 'eventsAgent();';
}

function getBXCityId($cityId)
{
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK, "UF_CITYID" => $cityId);
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID'));
    if ($arSection = $rsSections->GetNext()) {
        return $arSection['ID'];
    }
}

function checkEvents($cityId)
{
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK, "UF_CITYID" => $cityId);
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID'));
    if ($arSection = $rsSections->GetNext()) {
        $count = CIBlockSection::GetSectionElementsCount($arSection['ID'], array());
    }
    return $count;
}

function getRandomEvent($cityId)
{
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK, "=UF_CITYID" => $cityId);
    $arSelect = array(
//        'IBLOCK_ID',
        'ID',
        'NAME',
        'PREVIEW_PICTURE',
        'PREVIEW_TEXT',
        'DETAIL_PAGE_URL',
        'PROPERTY_VENUE',
//        'PROPERTY_SESSION.PROPERTY_DATE', // @WARNING вызывает баг дубликатов, либо это ссылочный параметр либо этого параметра нет
        'PROPERTY_DATES', // @WARNING вызывает баг дубликатов, либо это ссылочный параметр либо этого параметра нет
        'PROPERTY_EVENT_ID'
    );
    $rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID'));
    if ($arSection = $rsSections->GetNext()) {
        $rs = CIBlockElement::GetList(
            Array("RAND" => "DESC"),
            [
                "IBLOCK_ID" => EVENTS_IBLOCK,
                "SECTION_ID" => $arSection['ID']
            ],
            false,
            Array("nTopCount" => 10),
            $arSelect);

        $result = [];
        while ($resultEvent = $rs->GetNext()) {
            $resultEvent['IMG'] = CFile::GetPath($resultEvent['PREVIEW_PICTURE']);
            $resultEvent['CITY'] = $cityId;
            $result[] = $resultEvent;
        }
        return $result;
//        if ($resultEvent = $rs->GetNext()) {
//
//            $resultEvent['IMG'] = CFile::GetPath($resultEvent['PREVIEW_PICTURE']);
//            $resultEvent['CITY'] = $cityId;
//            return $resultEvent;
//        } else {
//            return false;
//        }
    } else {
        return false;
    }
}

function getCityName($id)
{
    $city = '';
    $pathDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/';
    $cities = json_decode(file_get_contents($pathDir . '/cities.json'), 1);
    foreach ($cities as $city) {
        if ($city['id'] == $id) {
            $city = $city['name'];
            break;
        }
    }
    return $city;
}


function getMonthDiff($date1, $date2)
{
    $begin = new DateTime($date1);
    $end = new DateTime($date2);
    $end = $end->modify('+1 month');

    $interval = DateInterval::createFromDateString('1 month');

    $period = new DatePeriod($begin, $interval, $end);
    $counter = 0;
    foreach ($period as $dt) {
        $counter++;
    }

    return $counter;
}

AddEventHandler("main", "OnBeforeUserUpdate", "sendConfirm");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "sendMail");
AddEventHandler("blog", "OnBeforeCommentAdd", "moderateComment");
AddEventHandler("blog", "OnBeforeCommentUpdate", "moderateComment");
AddEventHandler("main", "OnBeforeProlog", "getPersonalContent", 50);
AddEventHandler("main", "OnBeforeEventSend", "changeBlogMail");


function changeBlogMail(&$arFields)
{
    if (isset($arFields['BLOG_ID'])) {
        $arFields['WORD'] = '';
        $arFilter = array(
            "ID" => $arFields['BLOG_ID']
        );
        $arSelectedFields = array("ID", "UF_WORD");

        $dbBlogs = CBlog::GetList(
            array(),
            $arFilter,
            false,
            false,
            $arSelectedFields
        );

        if ($arBlog = $dbBlogs->Fetch()) {
            if (!empty($arBlog['UF_WORD'])) {
                $arFields['WORD'] = $arBlog['UF_WORD'];
            }
        }
    }
}

function moderateComment(&$arFields)
{
    $commentTextArray = explode(" ", $arFields['POST_TEXT']);
    $censStringUnencode = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/censlist.txt');
    $censString = iconv("windows-1251", "utf-8", $censStringUnencode);

    $censList = explode(", ", $censString);

    foreach ($commentTextArray as $k => &$word) {
        if (in_array($word, $censList)) {
            $commentTextArray[$k] = "*";
        }
    }
    $newCommentString = implode(" ", $commentTextArray);
    $arFields['POST_TEXT'] = $newCommentString;
}

function sendConfirm(&$arFields)
{
    $confirmCode = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"), -35);
    $link = "https://frutonyanya.ru/profile/security/?id=" . $arFields['ID'] . "&action=change&confirm=" . $confirmCode;
    $arFields['UF_CONFIRM'] = $confirmCode;


    //изменение пароля
    if ($arFields['PASSWORD']) {

        $rsUser = CUser::GetByID($arFields['ID']);
        $arUser = $rsUser->Fetch();
        $arEventFields = array(
            'EMAIL' => $arUser['EMAIL'],
            'NAME' => $arUser['NAME'],
            'LINK' => $link
        );
        CEvent::Send('CHANGE_PASS', 's1', $arEventFields);
    }

    // изменение логина
    if ($arFields['EMAIL'] && $arFields['LOGIN']) {
        //проверим, что такого логина нет в базе
        $rsUser = CUser::GetByLogin($arFields['LOGIN']);
        $arUser = $rsUser->Fetch();
        if (!$arUser['ID']) {

            //проверим, что логин изменился
            $rsUser = CUser::GetByID($arFields['ID']);
            $arUser = $rsUser->Fetch();

            if (($arUser['LOGIN'] != $arFields['LOGIN']) && ($arUser['EMAIL'] != $arFields['EMAIL'])) {
                $arFields['UF_CONFIRM'] = $confirmCode;
                $arEventFields = array(
                    'EMAIL' => $arFields['EMAIL'],
                    'OLD_EMAIL' => $arUser['EMAIL'],
                    'LINK' => $link
                );
                CEvent::Send('CHANGE_EMAIL', 's1', $arEventFields);
            }
        }
    }
}

function sendMail(&$arFields)
{
    if ($arFields['IBLOCK_ID'] == 19) {
        /**
         *смена раздела вопроса
         */

        // получаем текущий раздел верхнего уровня
        $resObj = CIBlockElement::GetByID($arFields['ID']);
        if ($arElementRes = $resObj->GetNext()) {
            $currentSectionId = $arElementRes['IBLOCK_SECTION_ID'];
        }
        //получаем текущий раздел верхнего уровня
        $navCurrent = CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $currentSectionId);
        if ($arCurrentSectionPath = $navCurrent->GetNext()) {
            $currentSection = $arCurrentSectionPath['ID'];
        }

        //получаем новый раздел верхнего уровня
        $nav = CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $arFields['IBLOCK_SECTION'][0]);
        if ($arSectionPath = $nav->GetNext()) {
            $newSection = $arSectionPath['ID'];
        }

        if ($currentSection != $newSection) {

            //проверка на смену раздела
            $res = CIBlockSection::GetByID($arFields['IBLOCK_SECTION'][0]);
            if ($arRes = $res->GetNext()) {
                $theme = $arRes['NAME'];
            }


            //получаем нового эксперта
            $rsSection = CIBlockSection::GetList(
                array(),
                array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $newSection),
                false,
                array("UF_CONSULTANT")
            );
            if ($arSection = $rsSection->GetNext()) {
                $newExpertId = intval($arSection['UF_CONSULTANT'][0]);
            }


            $rsNewExpert = CUser::GetByID($newExpertId);
            $arNewExpert = $rsNewExpert->Fetch();
            $arFields['MODIFIED_BY'] = $newExpertId;

            $elementUrl = "https://frutonyanya.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=19&type=userdata&ID=" . $arFields['ID'] . "&lang=ru";

            $arChangeConsultantEventFields = array(
                'EMAIL_TO' => $arNewExpert['EMAIL'],
                'NAME' => $arNewExpert['NAME'],
                'THEME' => $theme,
                'QUESTION' => $arFields['PREVIEW_TEXT'],
                'LINK' => $elementUrl
            );

            CEvent::Send('CHANGE_CONSULTANT', 's1', $arChangeConsultantEventFields);
        }


        /**
         *новый ответ
         */
        $ANSWER_STATUS = 39;

        if ($arFields['PROPERTY_VALUES'][86][0]['VALUE'] == $ANSWER_STATUS && !empty($arFields['DETAIL_TEXT'])) {  # статус изменен на "Отвечено"

            //получаем старое значение свойства
            $propsObj = CIBlockElement::GetProperty(
                $arFields['IBLOCK_ID'],
                $arFields['ID'],
                array(),
                array("CODE" => "STATUS")
            );
            if ($arProps = $propsObj->Fetch()) {
                $oldPropValue = $arProps["VALUE"];
            }


            if ($oldPropValue != $ANSWER_STATUS) {
                $questionText = $arFields['PREVIEW_TEXT'];

                // клиент
                $arSelect = array("CREATED_BY");
                $arFilter = array("ID" => $arFields['ID']);
                $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                if ($ob = $res->GetNextElement()) {
                    $fields = $ob->GetFields();
                    $userId = $fields['CREATED_BY'];
                }
                $rsUser = CUser::GetByID($userId);
                $arUser = $rsUser->Fetch();


                //эксперт
                foreach ($arFields['PROPERTY_VALUES'][84] as $expertIds) {
                    $expertId = $expertIds['VALUE'];
                }

                $rsExpert = CUser::GetByID($expertId);
                $arExpert = $rsExpert->Fetch();

                //получаем раздел
                $res = CIBlockSection::GetByID($arFields['IBLOCK_SECTION'][0]);
                if ($arRes = $res->GetNext()) {
                    $sectionCode = $arRes['CODE'];
                }


                $arEventFields = array(
                    'EMAIL' => $arUser['EMAIL'],
                    'NAME' => $arUser['NAME'],
                    'QUESTION_TEXT' => $questionText,
                    'EXPERT' => $arExpert['NAME'],
                    'LINK' => 'https://frutonyanya.ru/school/consultations/' . $sectionCode . '/'
                );

                CEvent::Send('CONSULTATION_ANSWER', 's1', $arEventFields);
            }
        }
    }
}

function getHighloadAgesIds($age)
{

    $agesArr = $agesIdsArr = array();
    if (CModule::IncludeModule("highloadblock")) {
        $ageTek = 0;
        $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_AGES)->fetch();
        $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $resData = $strEntityDataClass::getList(array(
            "select" => array("ID", "UF_AGE_TO", "UF_XML_ID"),
            "filter" => array(">=UF_AGE_TO" => $age),
            "order" => array("UF_AGE_TO" => "ASC"),
            "limit" => 100,
        ));
        while ($arItem = $resData->Fetch()) {
            // или просто >
            if ($ageTek == 0 || $age >= $ageTek || strstr($arItem["UF_XML_ID"], "0_")) {
                // если возраст ещё вписывается в текущий период, записываем его
                $agesArr[$arItem["UF_XML_ID"]] = $arItem["UF_AGE_TO"];
                $ageTek = $arItem["UF_AGE_TO"];
            } else {
                // текущий период больше возраста, прерываемся
                break;
            }
        }
        if (count($agesArr) > 0) {
            $agesIdsArr = array_keys($agesArr);
        }
    }
    return $agesIdsArr;
}

function getPersonalContent()
{
    global $USER;
    CModule::IncludeModule("iblock");

    if ($USER->IsAuthorized()) {

        $userID = $USER->GetID();

        // Ещё не определена и не записана в сессию статья на главную страницу
        if (!(isset($_SESSION["PERSONAL_CONTENT"]) and isset($_SESSION["PERSONAL_CONTENT"]["MAIN_ARTICLE"]) and $_SESSION["PERSONAL_CONTENT"]["MAIN_ARTICLE"]["ID"])) {

            //  Проверяем наличие детей и получаем дату рождения самого младшего
            $arSelect = array("ID", "IBLOCK_ID", "PROPERTY_USER", "PROPERTY_BIRTHDAY", "NAME");
            $currentChild = $currentChildId = false;
            $now_date = date("Y-m-d H:i:s");
            $arFilter = array(
                "IBLOCK_ID" => 18,
                "ACTIVE" => "Y",
                "=PROPERTY_USER" => $USER->GetID(),
                "<PROPERTY_BIRTHDAY" => $now_date
            );
            $res = CIBlockElement::GetList(array("PROPERTY_BIRTHDAY" => "DESC"), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNext()) {
                // проверяем что ребенку меньше 3-х лет и больше 0
                $birthday = strtotime($ob['PROPERTY_BIRTHDAY_VALUE']);
                $now = strtotime("now");

                $dateDiff = ($now - $birthday) / (60 * 60 * 24); // суток

                if ($dateDiff > 0 && $dateDiff < 1095) { // учитываем только детей до 3 лет
                    $currentChild = $ob;
                    if (!$currentChildId) {
                        $currentChildId = $ob['ID'];
                        // останавливаемся на самом младшем
                        break;
                    }
                }
            }
            $agesArr = $agesIdsArr = array();

            if ($currentChild and is_array($currentChild)) {

                $birthday = $currentChild["PROPERTY_BIRTHDAY_VALUE"];

                $date3 = $date1 = strtotime($birthday);
                $date2 = strtotime("now");
                $months = 0;

                while (($date3 = strtotime('+1 MONTH', $date3)) <= $date2) {
                    $months++;
                }

                if ($months > 0 && $months < 37) {
                    $age = $months;
                } elseif ($months < 37) {
                    // получаем возраст в доле месяца, кратной 0.25
                    $diff = $date2 - $date1;
                    $weeks = round(round(($diff / (604800)), 0) * 0.25, 2);

                    if ($weeks > 0) {
                        $age = $weeks;
                    }
                }

                if ($age && $age > 0) {
                    $agesIdsArr = getHighloadAgesIds($age);
                }
            }

            // Базовый фильтр статей
            $articleFilterBase = array(
                "IBLOCK_ID" => 10,
                "ACTIVE" => "Y",
                array(
                    "!ID" => CIBlockElement::SubQuery(
                        "ID",
                        array("IBLOCK_ID" => 10, "=PROPERTY_TAGS" => "mom")
                    )
                ),
                array(
                    "!ID" => CIBlockElement::SubQuery(
                        "ID",
                        array("IBLOCK_ID" => 10, "=PROPERTY_TAGS" => "pregnancy")
                    )
                )
            );

            $maxViewedTag = $maxViewedSection = 0;
            $maxViewedTagXmlID = "";
            // Получаем список самых читаемых тегов и самых читаемых статей (просматриваем только последние 100 записей)
            if (CModule::IncludeModule("highloadblock")) {
                $arViews = array("ALL" => array(), "BY_TAGS" => array(), "BY_SECTIONS" => array());
                $arHLBlockViews = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_VIEWS)->fetch();
                $obEntityViews = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlockViews);
                $strEntityDataClassViews = $obEntityViews->getDataClass();
                $resDataViews = $strEntityDataClassViews::getList(array(
                    "select" => array("ID", "UF_TAG_IDS", "UF_ART_SEC_ID"),
                    "filter" => array("UF_AUTH_USER_ID" => $userID),
                    "order" => array("UF_VIEW_DATE" => "DESC"),
                    "limit" => 100,
                ));
                // Записываем количество просмотров по всем тегам и по всем категориям
                while ($arItemView = $resDataViews->Fetch()) {
                    $arViews["ALL"] = $arItemView;
                    if ($arItemView["UF_TAG_IDS"] and is_array($arItemView["UF_TAG_IDS"])) {
                        foreach ($arItemView["UF_TAG_IDS"] as $tagId) {
                            if (!isset($arViews["BY_TAGS"][$tagId])) {
                                $arViews["BY_TAGS"][$tagId] = 1;
                            } else {
                                $arViews["BY_TAGS"][$tagId]++;
                            }
                        }
                    }
                    if ($arItemView["UF_ART_SEC_ID"]) {
                        $secId = $arItemView["UF_ART_SEC_ID"];
                        if (!isset($arViews["BY_SECTIONS"][$secId])) {
                            $arViews["BY_SECTIONS"][$secId] = 1;
                        } else {
                            $arViews["BY_SECTIONS"][$secId]++;
                        }
                    }
                }
                if (is_array($arViews["BY_TAGS"])) {
                    arsort($arViews["BY_TAGS"]);
                    // Проверяем, набрали ли мы 3 просмотра по тегам
                    foreach ($arViews["BY_TAGS"] as $tid => $tcnt) {
                        if ($tcnt >= 3) { // берем 1-ый тег с количеством просмотров > 3
                            $maxViewedTag = $tid;
                            break;
                        } elseif ($tcnt < 3) { // нет 3 просмотров
                            break;
                        }
                    }
                    if ($maxViewedTag > 0) {
                        // Получаем XML_ID тега для дальнейшей фильтрации
                        $arHLBlockTags = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_TAGS)->fetch();
                        $obEntityTags = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlockTags);
                        $strEntityDataClassTags = $obEntityTags->getDataClass();
                        $resDataTags = $strEntityDataClassTags::getList(array(
                            "select" => array("ID", "UF_XML_ID"),
                            "filter" => array("ID" => $maxViewedTag),
                            "order" => array("ID" => "ASC"),
                            "limit" => 1,
                        ));
                        if ($arItemTag = $resDataTags->Fetch()) {
                            $maxViewedTagXmlID = $arItemTag["UF_XML_ID"];
                            $_SESSION["PERSONAL_CONTENT"]["MAX_VIEWED_TAG"] = $maxViewedTag;
                            $_SESSION["PERSONAL_CONTENT"]["MAX_VIEWED_TAG_XML_ID"] = $maxViewedTagXmlID;
                        }
                    }
                }
                if (is_array($arViews["BY_SECTIONS"])) {
                    arsort($arViews["BY_SECTIONS"]);
                    // Проверяем, набрали ли мы 3 просмотра по категории
                    foreach ($arViews["BY_SECTIONS"] as $sid => $scnt) {
                        if ($scnt >= 3) { // берем 1-ую категорию с количеством просмотров > 3
                            $maxViewedSection = $sid;
                            $_SESSION["PERSONAL_CONTENT"]["MAX_VIEWED_SECTION"] = $maxViewedSection;
                            break;
                        } elseif ($scnt < 3) { // нет 3 просмотров
                            break;
                        }
                    }
                }
            }

            $article_found = false;
            for ($j = 1; $j <= 3; $j++) {

                // 3 шага
                // Если задан возраст ребенка: 1) возраст + рубрика + тэг; 2) возраст + рубрика; 3) возраст
                // Если возраст не указан: 1) рубрика + тэг; 2) рубрика; 3) -
                $articleFilter = $articleFilterBase;

                if (count($agesIdsArr) > 0) { // есть фильтр по возрасту ребенка
                    $articleFilter["PROPERTY_AGE"] = $agesIdsArr;
                } else {
                    if ($j == 3) { // Если возраст не указан, только 2 условия может быть
                        break;
                    }
                }
                if (
                    $j == 1 and $maxViewedTagXmlID
                    and $maxViewedTagXmlID != "" and $maxViewedTagXmlID != "mom" and $maxViewedTagXmlID != "pregnancy"
                ) { // есть фильтр по тегам
                    $articleFilter["PROPERTY_TAGS"] = array($maxViewedTagXmlID);
                }
                if (($j == 1 or $j == 2) and $maxViewedSection and $maxViewedSection != 0) { // есть фильтр по категориям
                    $articleFilter["IBLOCK_SECTION_ID"] = array($maxViewedSection);
                }

                $articleObj = CIBlockElement::GetList(
                    array("RAND" => "rand"),
                    $articleFilter,
                    false,
                    array("nTopCount" => 1),
                    array(
                        "ID",
                        "IBLOCK_ID",
                        "PROPERTY_TAGS",
                        "NAME",
                        "PROPERTY_AGE",
                        "IBLOCK_SECTION_ID"
                    )
                );

                if ($articlePersonal = $articleObj->GetNextElement()) {
                    $article = $articlePersonal->GetFields();
                    $_SESSION["PERSONAL_CONTENT"]["MAIN_ARTICLE"]["ID"] = $article["ID"];
                    $article_found = true;
                }
                if ($article_found == true) {
                    break;
                }
            }
        }
    }
}

function saveArticleView($USER, $ID)
{

    if ($USER and $USER->IsAuthorized()) {

        if (!isset($_SESSION["PERSONAL_VIEWS"][$ID])) {

            $userID = $USER->GetID();

            if ($userID) {
                // получаем данные по статье, чтобы записать в статистику
                $articleFilter = array(
                    "IBLOCK_ID" => 10,
                    "ACTIVE" => "Y",
                    "ID" => $ID
                );
                $articleObj = CIBlockElement::GetList(
                    array(),
                    $articleFilter,
                    false,
                    array("nTopCount" => 1),
                    array(
                        "ID",
                        "IBLOCK_ID",
                        "PROPERTY_TAGS",
                        "NAME",
                        "PROPERTY_AGE",
                        "IBLOCK_SECTION_ID"
                    )
                );

                if ($articleView = $articleObj->GetNextElement()) {

                    $article = $articleView->GetFields();
                    if ($article["PROPERTY_TAGS_VALUE"]) {
                        $props = $articleView->GetProperties();
                    }

                    // Дата, статья, раздел, ID user, теги
                    if (CModule::IncludeModule('highloadblock')) {
                        $tagsCodes = $tagsIds = array();
                        if ($props["TAGS"] and is_array($props["TAGS"]["VALUE"])) {
                            foreach ($props["TAGS"]["VALUE"] as $k => $tag) {
                                $tagsCodes[] = $tag;
                            }
                            $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_TAGS)->fetch();
                            $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
                            $strEntityDataClass = $obEntity->getDataClass();
                            $resData = $strEntityDataClass::getList(array(
                                "select" => array("ID", "UF_XML_ID"),
                                "filter" => array("UF_XML_ID" => $tagsCodes),
                                "order" => array("ID" => "ASC"),
                                "limit" => 100,
                            ));
                            while ($arItem = $resData->Fetch()) {
                                $tagsIds[$arItem["ID"]] = $arItem["ID"];
                            }
                        }

                        $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_VIEWS)->fetch();
                        $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
                        $entity_data_class = $obEntity->getDataClass();
                        $result = $entity_data_class::add(array(
                            'UF_ART_ID' => $article["ID"],
                            'UF_ART_SEC_ID' => $article["IBLOCK_SECTION_ID"],
                            'UF_VIEW_DATE' => date("d.m.Y H:i:s"),
                            'UF_AUTH_USER_ID' => $userID,
                            'UF_TAG_IDS' => (($article["PROPERTY_TAGS_VALUE"] and is_array($tagsIds)) ? $tagsIds : "")
                        ));
                    }
                }
            }
            $_SESSION["PERSONAL_VIEWS"][$ID] = $ID;
        }
    }
}

//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/agent_log.txt");
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Type\DateTime;

function AgentPeriodDeleteArtViews()
{
    $cnt_max = 100;
    $maxDate = date('Y-m-d');
    $max_date_str = date('Y-m-d H:i:s', strtotime($maxDate . ' - 1 month'));
    $max_date = new \Bitrix\Main\Type\DateTime($max_date_str, "Y-m-d H:i:s");

    //AddMessage2Log( "Периодический BX_CRONTAB:".BX_CRONTAB." BX_CRONTAB_SUPPORT:".BX_CRONTAB_SUPPORT);
    //AddMessage2Log( "Максимальное количество:".$cnt_max."\tМаксимальная дата:".$max_date_str."\r\n");

    // Удаляем записи из истории старше месяца и для каждого пользователя > 100
    // Дата, статья, раздел, ID user, теги
    if (CModule::IncludeModule('highloadblock')) {

        $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_ID_ART_VIEWS)->fetch();
        $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $usersCnt = array();
        $resDataU = $strEntityDataClass::getList(array(
            "select" => array("UF_AUTH_USER_ID", "CNT"),
            "filter" => array(),
            "runtime" => array(new Entity\ExpressionField("CNT", "COUNT(*)"))
        ));
        while ($arItemU = $resDataU->Fetch()) {
            // записей по пользователю больше, чем нужно, удаляем лишние
            //AddMessage2Log(date("Y-m-d H:i:s")."\tПользователь #".$arItemU["UF_AUTH_USER_ID"].", количество ".$arItemU["CNT"]."\r\n");
            if ($arItemU["CNT"] > $cnt_max) {
                // записей по пользователю больше, чем нужно, удаляем лишние
                //AddMessage2Log(date("Y-m-d H:i:s")."\tУдаляем записи > ".$cnt_max." для пользователя #".$arItemU["UF_AUTH_USER_ID"]);

                $resDataOverViews = $strEntityDataClass::getList(array(
                    "select" => array("ID", "UF_VIEW_DATE", "UF_AUTH_USER_ID"),
                    "filter" => array("UF_AUTH_USER_ID" => $arItemU["UF_AUTH_USER_ID"]),
                    "order" => array("UF_VIEW_DATE" => "ASC"),
                    "limit" => ($arItemU["CNT"] - $cnt_max), // количество свыше максимального
                ));
                $idForDelete = 0;
                while ($arItemDataOverViews = $resDataOverViews->Fetch()) {
                    // удаляем
                    $idForDelete = $arItemDataOverViews["ID"];
                    $result = $strEntityDataClass::delete($idForDelete);
                }
            }
        }

        // Получаем данные старше 30 дней
        $resDataOverDateViews = $strEntityDataClass::getList(array(
            "select" => array("ID", "UF_VIEW_DATE", "UF_AUTH_USER_ID"),
            "filter" => array("<UF_VIEW_DATE" => $max_date),
            "order" => array("UF_VIEW_DATE" => "ASC"),
            "limit" => 500,
        ));
        $idForDeletePerDate = 0;
        $iteration = 1;

        while ($arItemDataOverDateViews = $resDataOverDateViews->Fetch()) {
            if ($iteration == 1) {
                //AddMessage2Log(date("Y-m-d H:i:s")."\tУдаляем записи старше ".$max_date);
            }
            // удаляем
            $idForDeletePerDate = $arItemDataOverDateViews["ID"];
            $result = $strEntityDataClass::delete($idForDeletePerDate);
            $iteration++;
        }
    }
    return "AgentPeriodDeleteArtViews();";
}

function redirectSupportEmail()
{
    //
}

//end refactoring TODO remove after refatoring complete


// new event
function deleteOldAgent()
{
    $curDate = strtotime(date("d.m.Y", time()));
    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_LAST_DATE");
    $arFilter = Array("IBLOCK_ID" => EVENTS_IBLOCK,  '<PROPERTY_LAST_DATE' => date('Y-m-d'));
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $del = 0;
    $noDel = 0;
    while ($ob = $res->GetNext()) {
        $del++;
		//echo $ob['ID']."\n";
		CIBlockElement::Delete($ob['ID']);
    }
	var_dump($del . "!");
    return 'deleteOldAgent();';
}

function loadNewAllEvents($allEvents)
{
    $params = [
        'max_len' => '100', // обрезает символьный код до 100 символов
        'change_case' => 'L', // буквы преобразуются к нижнему регистру
        'replace_space' => '_', // меняем пробелы на нижнее подчеркивание
        'replace_other' => '_', // меняем левые символы на нижнее подчеркивание
        'delete_repeat_replace' => 'true', // удаляем повторяющиеся нижние подчеркивания
        'use_google' => 'false', // отключаем использование google
    ];
$i123 = 0;
    $result = array();
    /*перебираем разделы*/
    $arFilter = array('IBLOCK_ID' => EVENTS_IBLOCK);
    $rsSections = CIBlockSection::GetList(array("ID"=>"ASC"), $arFilter, false, array('ID', 'IBLOCK_ID', 'UF_CITYID','IBLOCK_SECTION_ID','NAME'));
    while ($arSection = $rsSections->GetNext()) {
        $cityId = $arSection['UF_CITYID'];
        /*получаем файл с событиями*/
        $eventsJSON = $_SERVER['DOCUMENT_ROOT'] . '/upload/events/new/afisha_files/' . $cityId . '/events.json';
        $events = json_decode(file_get_contents($eventsJSON), 1);

        foreach ($events as $event) {
            //проверка на "детский" раздел

            if (in_array("kids", $event['tags']) || in_array("kids", $event['session_tags']) ) {
                if (!key_exists($event['id'], $allEvents[$arSection["ID"]])) {
                    $venue = getVenueById($cityId, $event['id']);               // местоположение
                    $PROP = [
                        'EVENT_ID' => $event['id'],                             // ID события
                        'SESSION' => getSessionsById($cityId, $event['id']),    // ID сеанса
                        'VENUE' => $venue['title'],
                        'VENUE_LINK' => $venue['link'],                         // Ссылка на место проведения
                        'VENUE_ADDRESS' => $venue['address'],                   // Адрес места проведения
                        'VENUE_METRO' => $venue['metro'],                       // Адрес места проведения
                        'VENUE_LOCATION' => implode(',', $venue['location']),   // Адрес места проведения
                        'EVENT_URL' => $event['url'],                           // Url в афише
                        'DATES' => $event['dates'],                             // даты
                        'LAST_DATE' => array_pop($event['dates']),
                        'COID' => $event['coid'],                               // ID кинопоиска
                        'SELECTIONS' => getSelections($cityId, $event['selections']),   // подборки
                        'DURATION' => $event['duration'],
                        'DIRECTOR' => getDirector($event['persons']),
                        'YEAR' => $event['year'],
                        'ACTORS' => getActors($event['persons']),
                        'COUNTRY' => implode(',', $event['countries'])
                    ];

                    $images = [
                        'image' => $event['image']['origin_url'],
                        'sub' => []
                    ];
                    $imagesCounter = 0;
                    if (!empty($event['image_gallery'])) {
                        foreach ($event['image_gallery'] as $image) {
                            if ($imagesCounter == 4) {
                                break;
                            }
                            if ($image['type'] == 'image' && $image['sub_type'] != 'other') {
                                $images['sub'][] = $image;
                                $imagesCounter++;
                            }
                        }
                    }

                    $PROP['IMAGES_LINKS'] = json_encode($images);

                    if ($event['ratings']['kinopoisk'] > 0) {
                        $PROP['KINOPOISK_RATE'] = $event['ratings']['kinopoisk'];
                    }

                    if (!empty($event['prices'])) {
                        foreach ($event['prices'] as $price) {
                            if ($price['currency_code'] == 'rub') {
                                $PROP['PRICE'] = substr($price['value'], 0, -2) . ' ₽';
                                break;
                            }
                        }
                    }

                    $el = new \CIBlockElement;
                    $description = strip_tags($event['description']);
                    $arLoadProductArray = [
                        'IBLOCK_SECTION_ID' => $arSection["ID"],
                        'IBLOCK_ID' => $arSection["IBLOCK_ID"],
                        'NAME' => $event['title'],
                        'CODE' => \CUtil::translit($event['title'], 'ru', $params),
                        'PROPERTY_VALUES' => $PROP,
                        'ACTIVE' => 'Y',
                        'PREVIEW_TEXT' => substr($description, 0, 50) . '...',
                        'DETAIL_TEXT' => $description
                    ];

                    //Сохранение дополнительных изображений
                    $el->Add($arLoadProductArray);

//                    if($PRODUCT_ID = $el->Add($arLoadProductArray))
//                        echo "New ID: ".$PRODUCT_ID;
//                    else
//                        echo "Error: ".$el->LAST_ERROR;
//die();
                }


            }
        }
    }


//    dump($i123);
    return true;
}


function loadEventsAgent()
{
    $start = microtime(true);
    downloadEvents(); // получаем новую выгрузку
    \Tagency\Frutonyanya\Events::getInstance()->createSections(); //создаём разделы
    $res = \Tagency\Frutonyanya\Events::getInstance()->getAll();//получаем все события
    $allImportedEvents = loadNewAllEvents($res); //загружаем все события
    dump('Время выполнения скрипта: ' . round(microtime(true) - $start, 4) . ' сек.');
    return 'loadEventsAgent();';
}

// new event end


AddEventHandler("main", "OnAfterUserLogin", Array("userClass", "OnAfterUserLoginHandler"));
AddEventHandler("main", "OnAfterUserRegister", Array("userClass", "OnAfterUserRegisterHandler"));

class userClass
{
    function OnAfterUserRegisterHandler(&$fields)
    {
        if ($fields['USER_ID'] > 0) {
/*            
$rsUser = CUser::GetByID($fields['USER_ID']);
            $arUser = $rsUser->Fetch();
            \Traffic\Bonus\Lib\User::getContext('reg', $arUser["ID"], $arUser["EMAIL"]);
*/
        }
    }

    function OnAfterUserLoginHandler(&$fields)
    {
        if ($fields['USER_ID'] > 0) {
/*
            $rsUser = CUser::GetByID($fields['USER_ID']);
            $arUser = $rsUser->Fetch();
            \Traffic\Bonus\Lib\User::getContext('auth', $arUser["ID"], $arUser["EMAIL"]);
*/
        }
    }
}
