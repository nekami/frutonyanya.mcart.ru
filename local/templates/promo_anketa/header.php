<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="ru">
        
    <head>	
        <meta charset=<?=LANG_CHARSET?>>
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
				
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />				
        <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/app.min.js" );?>
	    <? CJSCore::Init(["jquery", "ajax"]); ?>
		<?$APPLICATION->ShowHead();?>	
		<? $APPLICATION->AddHeadScript('https://www.google.com/recaptcha/api.js?render='.SITE_KEY); ?>
		<title><?$APPLICATION->ShowTitle()?></title>
    </head>
        
    <body class="page">	
        <div class="page__wrapper">