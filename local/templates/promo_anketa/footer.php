<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
			<div class="footer">
				<p class="footer_text">© 2020, ««ФрутоНяня»» Все права защищены.</p>
			</div>
								
			<div class="popup">
				<div class="popup_wrap">
					<img class="popup_img js-ok-img ok-img" src="<?=SITE_TEMPLATE_PATH?>/img/popup/ok.svg" />
					<img class="popup_img js-err-img err-img" src="<?=SITE_TEMPLATE_PATH?>/img/popup/err.svg" />
					<div class="popup_title js-title">Форма успешно отправлена!</div>
					<div class="popup_text js-text">После проверки Ваших данных сертификат будет отправлен на e-mail, указанный при регистрации. Срок рассылки призов - с 01.11.2020 по 31.12.2020</div>
					<!--if data-href===' ' ? close popup : переход по url, например data-href="/lc.html"-->
					<button class="popup_close" type="button" data-href="/promo/winners-form/">Закрыть</button>
				</div>
			</div>
			
		</div>
    </body>
</html>