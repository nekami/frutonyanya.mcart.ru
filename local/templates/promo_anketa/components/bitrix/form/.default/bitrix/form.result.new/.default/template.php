<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<section class="form_section">
    <div class="form_wrapper">
	
		<div class="form_header">
			<div class="form_top_block">
                <a href="/promo/" class="form_top_head">
                    <img class="form_logo" src="<?=SITE_TEMPLATE_PATH?>/img/form/logo20.svg" alt="logo" />
                </a>
                <div class="form_bot_head">

			<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

			<?//=$arResult["FORM_NOTE"]?>

			<? if ($arResult["isFormNote"] != "Y") { ?>
			
			<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
			{
			/***********************************************************************************
								form header
			***********************************************************************************/
			if ($arResult["isFormTitle"]) { ?>
				<h1 class="form_title"><?=$arResult["FORM_TITLE"]?></h1>
			<? } //endif ;

				if ($arResult["isFormImage"] == "Y")
				{
				?>
				<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
				<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
				<?
				} //endif
				?>

				<p class="form_bot_text"><?=$arResult["FORM_DESCRIPTION"]?></p>
					
				<?
			} // endif
				?>
			</div>
		</div>
	</div>	
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>
		<div class="form"><?=$arResult["FORM_HEADER"]?>
		
			<?/* foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) { ?>				
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
					<span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span>
				<?endif;?>
			<? } */ ?>
			
			<div class="form_item_title first">Личная информация</div>			
            <div class="form_item">
                <div class="form_item_wrap first">
                    <p class="form_item_name">ФИО</p>
                    <div class="form_inputs_wrap">
					
                        <?=$arResult["QUESTIONS"]["name"]["HTML_CODE"];?>
						<div class="error_text">Поле обязательно для заполнения</div>
						
                        <?=$arResult["QUESTIONS"]["familiy"]["HTML_CODE"];?>
						<div class="error_text">Поле обязательно для заполнения</div>
						
                        <?=$arResult["QUESTIONS"]["otchestvo"]["HTML_CODE"];?>
						<div class="error_text">Поле обязательно для заполнения</div>
						
                    </div>
                </div>
            </div>			
            <div class="form_item last">
                <div class="form_item_wrap">
                    <p class="form_item_name">Контакты</p>
                    <div class="form_inputs_wrap">
                        <?=$arResult["QUESTIONS"]["phone"]["HTML_CODE"];?>
						
                        <?=$arResult["QUESTIONS"]["email"]["HTML_CODE"];?>
						<div class="error_text" style="text-align: right;">Поле обязательно для заполнения</div>
						
                        <?=$arResult["QUESTIONS"]["address"]["HTML_CODE"];?>
                        <span class="area_help_text">
                            <b>Пример заполнения: </b>654066, г. Санкт-Петербург, ул. Грдины, д. 12, кв. 61
                        </span>
                    </div>
                </div>
            </div>
			
			<!---------------------------------------------------------------------------------->
			
            <div class="form_item_title second">Документы</div>												
            <div class="form_item passport">			
                <div class="form_item_wrap">				
                    <p class="form_item_name">Паспорт</p>                    
					<div class="form_inputs_wrap">
					
						<?=$arResult["QUESTIONS"]["seria"]["HTML_CODE"];?>
						<div class="error_text" style="margin-top: -40px;">Поле обязательно для заполнения</div>	
						
						<?=$arResult["QUESTIONS"]["number"]["HTML_CODE"];?> 
						<div class="error_text">Поле обязательно для заполнения</div>
						
                    </div>					
                </div>                                                        
				<div class="form_item_wrap">				
					<p class="form_item_name">Скан-копия паспорта:</p>				
                    <div class="form_inputs_wrap last">	
						<? foreach ($arResult["QUESTIONS"]["span_pasport"]["STRUCTURE"] as $item) { ?>
							<label class="form_input label_passport">						
								<?=$item["HTML_CODE"];?>															
								<span class="js-input-text image-name">Файл<?if ($item["REQUIRED"] == "Y"):?>*<?endif;?></span>
							</label>   							
							<span class="label_passport_help">2,3 страницы паспорта. <b>Загружемые форматы: </b>.png, .jpeg, .jpg, .pdf</span>
							<div class="error_text">Поле обязательно для заполнения</div>							
						<? } ?>  
                    </div>
                </div>
            </div>            
			<div class="form_item snils">			
                <div class="form_item_wrap">                                                                
					<p class="form_item_name">СНИЛС</p>					
                    <div class="form_inputs_wrap">
                        <?=$arResult["QUESTIONS"]["snils"]["HTML_CODE"];?>
                    </div>
                </div>				
                <div class="form_item_wrap">                                                                
					<p class="form_item_name">Скан-копия СНИЛС:</p>
					<div class="form_inputs_wrap last">                        
						<label class="form_input label_passport" for="snils-copy">
                            <?=$arResult["QUESTIONS"]["skan_snils"]["HTML_CODE"];?>
                            <span class="js-input-text image-name">Файл</span>
                        </label>                        
						<span class="label_passport_help"><b>Загружемые форматы: </b>.png, .jpeg, .jpg, .pdf</span>
                    </div>
                </div>				
            </div>                                                
			<div class="form_item inn">			
                <div class="form_item_wrap">                    
					<p class="form_item_name">ИНН
                        <div class="form_inputs_wrap">
                            <?=$arResult["QUESTIONS"]["inn"]["HTML_CODE"];?>
							<div class="error_text">Поле обязательно для заполнения</div>
                        </div>
                    </p>
                </div>				
                <div class="form_item_wrap">				
                    <p class="form_item_name">Скан-копия ИНН:</p>                 
					<div class="form_inputs_wrap long">					
                        <label class="form_input label_passport" for="inn-copy">						
                            <?=$arResult["QUESTIONS"]["skan_inn"]["HTML_CODE"];?>							
                            <span class="js-input-text image-name">Файл*</span>
                        </label>												
                        <span class="label_passport_help"><b>Загружемые форматы: </b>.png, .jpeg, .jpg, .pdf</span>
						<div class="error_text">Поле обязательно для заполнения</div>
                    </div>
                </div>
            </div>
            <div class="form_item comment">
                <div class="form_item_wrap">
                    <p class="form_item_name">Добавить<br/>комментарий</p>
                    <div class="form_inputs_wrap">
                        <?=$arResult["QUESTIONS"]["comment"]["HTML_CODE"];?>
                    </div>
                </div>
            </div> 
			<div class="form_item send" style="padding-bottom: 15px; padding-top: 10px;">
                <div class="form_item_wrap">
					<div class="form_footer" data-name="g-recaptcha-response">									
						<input type="hidden" id="g-recaptcha-response_4" name="g-recaptcha-response" value="">
						<div class="error_text" style="text-align: center;">От роботов письма не принимаются.</div>
					</div>
				</div>
            </div>			
			<div class="form_item send">
                <div class="form_item_wrap">
                    <div class="form_footer">
                        <div class="form_bot_info">
							<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
						</div>
                        <div class="form_checkbox_wrap">						
                            <input class="checkbox" type="checkbox" name="AGREE" value="N" checked="checked" id="checkbox" />
                            <label class="checkbox_label" for="checkbox"><span>
								Я даю согласие на <a class="checkbox_rules" href="#" target="_blank">обработку моих персональных данных</a>
                            </span></label>
                        </div>						
						<input class="form_button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />						
                    </div>
                </div>
            </div>			
		</div>
	</div>
</section>
<?=$arResult["FORM_FOOTER"]?>
<? } ?>