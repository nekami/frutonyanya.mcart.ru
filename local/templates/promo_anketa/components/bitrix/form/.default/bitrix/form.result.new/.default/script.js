$(function() 
{
	ClearErrorText();

	$(document).on('click', 'input[name="web_form_submit"]', function(event) 
	{
		event.preventDefault();
		
		var error = false;
		
		$('form[name="SIMPLE_FORM_1"]').find('input').each(function(index, value) 
		{
			if (!$(value).val() && $(value).attr("required") == "required")
			{
				if ($(value).attr("type") == "file")				
					$(value).parent("label").addClass("errortext").next().next('div.error_text').show();
				else $(value).addClass("errortext").next('div.error_text').show();
				
				error = true;
			}
		});			
			
		if (!error)		
		{
			var formData = new FormData($('form[name="SIMPLE_FORM_1"]')[0]);
			
			$.ajax({
			  url: '/local/templates/promo_anketa/components/bitrix/form/.default/bitrix/form.result.new/.default/ajax.php',
			  data: formData,
			  cache: false,
			  contentType: false,
			  processData: false,			  
			  type: 'POST',			  
			  success: function(data) 
			  {
				if (data.error)
					$('form[name="SIMPLE_FORM_1"]').find('[data-name="g-recaptcha-response"] .error_text').show();
				else
				{
					$('.popup').addClass('active');		
					$('.page__wrapper.filter').addClass('filter');					
					$('.page.scroll').addClass('scroll');
				}
			  },			  
			  error: function(data) {
				console.log('Ошибка ajax-скрипта');
			  }
			});
		} else ScrollTopErrorText();
	});
	
});

function ClearErrorText ()
{
	$(document).on('input', 'input', function () { 	  
		if ($(this).hasClass('errortext'))
		{			
			$(this).removeClass("errortext").next('div.error_text').hide();			
		}		
	 });
}

function ScrollTopErrorText ()
{
	$('html, body').animate({
		scrollTop: $($('.errortext')[0]).offset().top
	}, 500);			
}