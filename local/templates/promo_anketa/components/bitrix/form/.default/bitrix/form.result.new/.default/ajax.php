<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if ( !empty($_POST) ) 
{
	if ($_POST['g-recaptcha-response'])
	{	
		$Return = getCaptcha($_POST['g-recaptcha-response']);		
		
		if ($Return->success != true && $Return->score <= 1)
		{
			echo json_encode(["error" => true]);
			die;
		}			
	}	
	
	CModule::IncludeModule("form");
	
	$WEB_FORM_ID = 1;
	
	if ($RESULT_ID = CFormResult::Add($WEB_FORM_ID, $_POST))	
	{		
		CFormCRM::onResultAdded($WEB_FORM_ID, $RESULT_ID);
		CFormResult::SetEvent($RESULT_ID);
		CFormResult::Mail($RESULT_ID);
		
		if (CFormResult::Mail($RESULT_ID))
		{
			echo json_encode(["error" => false]);
		}
		else
		{
			echo json_encode(["error" => true]);
		}			
	}	
}	
?>