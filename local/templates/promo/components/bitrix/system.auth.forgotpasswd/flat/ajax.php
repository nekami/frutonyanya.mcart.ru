<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if(!check_bitrix_sessid()) {
	echo json_encode(array('success' => 0));
	die();
}

if ($_POST['g-recaptcha-response'])
{		
	/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
	$Return = getCaptcha($_POST['g-recaptcha-response']);
			
	/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
	if ($Return->success != true && $Return->score <= 1)
	{
		echo json_encode(['error' => true]);
		die;
	}			
}

if ( !empty($_POST['EMAIL']) ) 
{
	$EMAIL = trim($_POST['EMAIL']);	
	
	CModule::IncludeModule('main');	
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;
	
	$rsUsers = CUser::GetList(($by="UF_SUM_BONUS"), ($order="desc"), ['EMAIL' => $EMAIL], ['FIELDS' => ['ID', 'CHECKWORD', 'NAME']]);

	if ($arUser = $rsUsers->Fetch()) 
	{
		if (!empty($arUser['CHECKWORD']))
		{
			CEvent::Send("USER_PASS_REQUEST", ["s1", "s2"], ["NAME" => $arUser['NAME'], "EMAIL" => $EMAIL, "URL_LOGIN" => $EMAIL, "CHECKWORD" => $arUser['CHECKWORD']], "N", 3);
		
			echo json_encode(['success' => true]);	
		}
		else
		{
			echo json_encode(['error' => true]);	
		}
	}
} 	


?>