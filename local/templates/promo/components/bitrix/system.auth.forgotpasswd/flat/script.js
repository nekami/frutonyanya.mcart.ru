$(function() 
{	
	$('input[name="send_account_info"]').click(function(event)
	{
		event.preventDefault();	
		
		var email  = $('input[name="USER_LOGIN_2"]').val();
		var sessid = $('input[name="sessid"]').val();

		if (!email)
		{
			$('input[name="USER_LOGIN_2"]').addClass("errortext").next('div.error_text').show(); 
		} 
		else 
		{	
			var me = this;
	
			$.ajax({
			  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
			  data: {'EMAIL': email, 'sessid': sessid},
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				  if (data.success)
				  {
					  $(me).parents('.popup.active').removeClass('active');		
					  
					  $('.popup[data-popup="forgotpasswd"]').addClass('active');	
					  
					  
					  
					  //$('.page__wrapper.filter').removeClass('filter');					
					  //$('.page.scroll').removeClass('scroll');
					  
					  
					  /*
					  				  
					  $('.page__wrapper').addClass('filter');					  
					  $('.page').addClass('scroll');
					  */
				  }
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 