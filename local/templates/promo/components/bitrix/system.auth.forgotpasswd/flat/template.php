<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<div class="popup__form">

<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
	<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

	<form name="bform" method="post" target="_top" class="regForm" action="<?=$arResult["AUTH_URL"]?>">
	
		<?=bitrix_sessid_post()?>
	
		<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">
		
		<div class="regForm__row regForm__row--popup">
            <label class="regForm__label">
                <div class="regForm__input-name"><?=GetMessage("AUTH_LOGIN_EMAIL")?></div>
                <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
					<input type="email" name="USER_LOGIN_2" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" class="regForm__input" placeholder="Ваш e-mail"/>
					<div class="error_text">Поле обязательно для заполнения</div>
					<input type="hidden" name="USER_EMAIL_2" />
                </div>
            </label>
			
			<?/*<div class="bx-authform-note-container"><?echo GetMessage("forgot_pass_email_note")?></div>*/?>
        </div>
		
		<div class="regForm__captcha regForm__captcha--popup" data-name="g-recaptcha-response">
			<input type="hidden" id="g-recaptcha-response_3" name="g-recaptcha-response" value="">				
			<div class="error_text">От роботов письма не принимаются.</div>
		</div>
		
		<input type="submit" class="regForm__submit regForm__submit--full regForm__submit--margin button button--blue" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
		
		<a class="regForm__link js-popup-open" href="#" data-popup="auth">вернуться назад</a>
		
	</form>

</div>

<script type="text/javascript">
document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN_2.value;};
document.bform.USER_LOGIN_2.focus();
BX.message({TEMPLATE_PATH: '<?=$this->GetFolder()?>'});
</script>