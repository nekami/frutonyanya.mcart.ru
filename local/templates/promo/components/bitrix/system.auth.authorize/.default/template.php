<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>

<div class="reg lc__reg">
    <div class="container reg__container">
        <div class="popup__form form__page">
		
			<?if($arResult["AUTH_SERVICES"]):?>			
				<div class="popup__header">
                  <div class="popup__title"><?=GetMessage("AUTH_TITLE")?></div>
                </div>				
			<?endif?>
				
			<form name="form_auth" class="regForm" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
				<?if (strlen($arResult["BACKURL"]) > 0):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
								
				<div class="regForm__row regForm__row--popup">
                    <label class="regForm__label">
                        <div class="regForm__input-name">Адрес электронной почты</div>
                        <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
                          <input class="regForm__input" type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
						  <div class="error_text">Поле обязательно для заполнения</div>
                        </div>
                    </label>
                    <label class="regForm__label">
                        <div class="regForm__input-name">Пароль</div>
                        <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>						  
							<input class="regForm__input" type="password" name="USER_PASSWORD_10" maxlength="255" autocomplete="off" />
							<div class="error_text">Поле обязательно для заполнения</div>
                        </div>
                    </label>
                </div>
		
				<div class="regForm__captcha regForm__captcha--popup" data-name="g-recaptcha-response">
					<input type="hidden" id="g-recaptcha-response_6" name="g-recaptcha-response" value="">				
					<div class="error_text">От роботов письма не принимаются.</div>
				</div>
                    
				<div class="regForm__btns">
                    <button class="regForm__auth button button--blue" type="submit" id="authSubmit2">Войти</button>
					<a class="button regForm__reg" href="/promo/registration/">Регистрация</a>
                </div>
				<a class="regForm__link js-popup-open" href="#" data-popup="changePassword">Забыли пароль</a>
		
			</form>

			<script type="text/javascript">
			<?if (strlen($arResult["LAST_LOGIN"])>0):?>
			try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
			<?else:?>
			try{document.form_auth.USER_LOGIN.focus();}catch(e){}
			<?endif?>
			</script>
			
			<div class="regForm__social">
				<div class="regForm__social-title">Или войдите через <br/>социальные сети</div>		
					
				<div class="regSocials">
					<ul class="regSocials__list">                         
						<? foreach($arResult["AUTH_SERVICES"] AS $service) { ?>
						
							<? 
							$prefix = '';
							switch ( strtolower($service["ID"]) ) {
								case 'vkontakte': $prefix = "vk"; break;
								case 'facebook': $prefix = "fb"; break;
								case 'odnoklassniki': $prefix = "ok"; break;
							} ?>
							  
							<li class="regSocials__item"><a  class="regSocials__link" title="<?=$service["ID"]?>" href="javascript:void(0)" onclick="$('#bx_auth_serv_form<?=$service["ID"]?> a').eq(0).click();">
												
								<div class="regSocials__icon regSocials__icon--<?=$prefix?>"></div>
											
							</a></li>
								
						<? } ?>					
					</ul>
				</div>

				<div style="display:none">
					<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
						array(
							"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
							"AUTH_URL" => $arResult["AUTH_URL"],
							"POST" => $arResult["POST"],
							"POPUP" => "N",
							"SUFFIX" => "form",
						),
						$component,
						array("HIDE_ICONS" => "Y")
					); ?>
				</div>
			</div>
		  
        </div>
	</div>
</div>