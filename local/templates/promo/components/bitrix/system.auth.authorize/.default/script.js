$(function() 
{	
	ClearErrorText();

	$(document).on('click', '#authSubmit2', function(event) 
	{
		event.preventDefault();		
		
		var auth_form  = $('input[name="AUTH_FORM"]').val();		
		var login      = $('input[name="USER_LOGIN"]').val();
		var pass       = $('input[name="USER_PASSWORD_10"]').val();		
		var recaptcha  = $('input[name="g-recaptcha-response"]').val();

		if (!login || !pass)
		{
			if (!login)
			{ 
				$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').show(); 
			}
					
			if (!pass) 
			{ 
				$('input[name="USER_PASSWORD_10"]').addClass("errortext").next('div.error_text').show();            
			}			
		} 
		else 
		{		
			$.ajax({
			  url: '/local/templates/promo/ajax/authorize.php',
			  data: { 'AUTH_FORM': auth_form, 
					  'USER_LOGIN': login, 					  
					  'USER_PASSWORD': pass,
					  'recaptcha': recaptcha
			  },
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				if (data.login) 
				{ 
					$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').html(data.login).show();
				}
				
				if (data.pass) 
				{ 
					$('input[name="USER_PASSWORD_10"]').addClass("errortext").next('div.error_text').html(data.pass).show();
				}	

				if (data.recaptcha) 
				{ 
					$('input[name="g-recaptcha-response"]').next('.error_text').show();
				}				
				  
				if (data.success) 
				{ 
					location.reload();
				}
				
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 