$(function() 
{	
	ClearErrorText();

	$(document).on('click', '#authorize_user', function(event) 
	{
		event.preventDefault();		
		
		var auth_form = $('input[name="AUTH_FORM"]').val();		
		var login     = $('input[name="USER_LOGIN"]').val();
		var pass      = $('input[name="USER_PASSWORD_1"]').val();
		//var sessid   =  $('input[name="sessid"]').val();
		var recaptcha          = $('input[name="g-recaptcha-response"]').val();

		if (!login || !pass)
		{
			if (!login)
			{ 
				$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').show(); 
			}
					
			if (!pass) 
			{ 
				$('input[name="USER_PASSWORD_1"]').addClass("errortext").next('div.error_text').show();            
			}			
		} 
		else 
		{		
			$.ajax({
			  url: '/local/templates/promo/ajax/authorize.php',
			  data: { 'AUTH_FORM': auth_form, 
					  'USER_LOGIN': login, 					  
					  'USER_PASSWORD': pass,
					  'recaptcha': recaptcha
					  //'sessid': sessid
			  },
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				if (data.login) 
				{ 
					$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').html(data.login).show();
				}
				
				if (data.pass) 
				{ 
					$('input[name="USER_PASSWORD_1"]').addClass("errortext").next('div.error_text').html(data.pass).show();
				}		
				  
				if (data.success) 
				{ 
					location.reload();
				}
				
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 