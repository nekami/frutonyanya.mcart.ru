<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();  ?>
<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('<?=SITE_KEY?>').then(function(token) 
		{
			document.getElementById('g-recaptcha-response_1').value=token;
		});
	});
</script>