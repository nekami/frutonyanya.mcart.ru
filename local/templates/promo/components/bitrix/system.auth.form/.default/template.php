<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init();
?>

<div class="popup__form">

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<? if($arResult["FORM_TYPE"] == "login"):?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" class="regForm">

<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />	
<?endif?>

<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />	
<?endforeach?>

	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	
	<div class="regForm__row regForm__row--popup">	
		<label class="regForm__label">
            <div class="regForm__input-name"><?=GetMessage("AUTH_LOGIN")?></div>
            <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
				<input type="email" name="USER_LOGIN" maxlength="50" value="" size="17" class="regForm__input" required="required" placeholder="Ваш e-mail"/>
				<div class="error_text">Поле обязательно для заполнения</div>
				<script>
					BX.ready(function() {
						var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
						if (loginCookie)
						{
							var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
							var loginInput = form.elements["USER_LOGIN"];
							loginInput.value = loginCookie;
						}
					});
				</script>
            </div>
        </label>
	
		<label class="regForm__label">		
            <div class="regForm__input-name"><?=GetMessage("AUTH_PASSWORD")?></div>			
            <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>			
                <input type="password" name="USER_PASSWORD_1" maxlength="255" size="17" autocomplete="off" class="regForm__input" required="required" placeholder="Ваш пароль"/>
				<div class="error_text">Поле обязательно для заполнения</div>
            </div>			
        </label>		
	</div>
	
	<div class="regForm__captcha regForm__captcha--popup" data-name="g-recaptcha-response">
		<input type="hidden" id="g-recaptcha-response_1" name="g-recaptcha-response" value="">				
	    <div class="error_text">От роботов письма не принимаются.</div>
	</div>
				
	<div class="regForm__btns">		
		<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" class="regForm__auth button button--blue" id="authorize_user"/>
					  
		<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
			<noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow" class="button regForm__reg"><?=GetMessage("AUTH_REGISTER")?></a></noindex>
		<?endif?>
    </div>	
						
	<noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" class="regForm__link js-popup-open" data-popup="changePassword"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex>
				
	<? if ($arResult["AUTH_SERVICES"]) { ?>
	
	<div class="regForm__social">
		<div class="regForm__social-title">Или войдите через <br/>социальные сети</div>		
			
		<div class="regSocials">
            <ul class="regSocials__list">                         
				<? foreach($arResult["AUTH_SERVICES"] AS $service) { ?>
				
					<? 
					$prefix = '';
					switch ( strtolower($service["ID"]) ) {
						case 'vkontakte': $prefix = "vk"; break;
						case 'facebook': $prefix = "fb"; break;
						case 'odnoklassniki': $prefix = "ok"; break;
					} ?>
					  
					<li class="regSocials__item"><a  class="regSocials__link" title="<?=$service["ID"]?>" href="javascript:void(0)" onclick="$('#bx_auth_serv_form<?=$service["ID"]?> a').eq(0).click();">
										
						<div class="regSocials__icon regSocials__icon--<?=$prefix?>"></div>
									
					</a></li>
						
				<? } ?>					
            </ul>
        </div>

		<div style="display:none">
			<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
				array(
					"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
					"AUTH_URL" => $arResult["AUTH_URL"],
					"POST" => $arResult["POST"],
					"POPUP" => "N",
					"SUFFIX" => "form",
				),
				$component,
				array("HIDE_ICONS" => "Y")
			); ?>
		</div>
			
	</div>
	<? } ?>	
</form>

<? if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>

<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?echo GetMessage("auth_form_comp_otp")?><br />
			<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
		</tr>
<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
		<tr>
			<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
		</tr>
<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
		</tr>
	</table>
</form>

<?
else:
?>

<form action="<?=$arResult["AUTH_URL"]?>">
	<table width="95%">
		<tr>
			<td>
				<?=$arResult["USER_NAME"]?><br />
				[<?=$arResult["USER_LOGIN"]?>]<br />
				<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
			</td>
		</tr>
		<tr>
			<td>
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
			</td>
		</tr>
	</table>
</form>
<?endif;?>


</div>

<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
</script>