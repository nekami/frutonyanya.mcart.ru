<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
	
    <div class="regSocials">
        <ul class="regSocials__list">		
		<? foreach($arParams["~AUTH_SERVICES"] as $service) { ?>
			<li class="regSocials__item">			
				<a  class="regSocials__link" title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="BxShowAuthFloat('<?=$service["ID"]?>', '<?=$arParams["SUFFIX"]?>')">
				
					<? switch ($service["ICON"]) { 						
						case "odnoklassniki": $prefix = "ok"; break;						
						case "vkontakte": $prefix = "vk"; break;						
						case "facebook": $prefix = "fb"; break;						
					} ?>				
					
					<div class="regSocials__icon regSocials__icon--<?=$prefix?>"></div>
								
				</a>			
			</li>
		<? } ?>							  
        </ul>
    </div>
	
	
	
	

