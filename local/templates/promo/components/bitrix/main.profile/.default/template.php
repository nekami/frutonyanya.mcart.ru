<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult["SHOW_SMS_FIELD"] == true) CJSCore::Init('phone_auth'); ?>
	  
      <div class="lc">
        <div class="lc__bg"></div>
        <div class="lc__decor1"></div>
        <div class="lc__decor2"></div>
        <div class="container lc__container">
          <div class="lc-header">
            <div class="lc-header__top">
              <div class="lc-header__title"><?=GetMessage("PERSONAL_ACCOUNT");?></div>			  
			  
              <div class="lc-header__name"><?=$arResult["arUser"]["NAME"];?></div>
			  
            </div>
            <div class="lc-header__bottom">
			
              <div class="lc-header__phone lc-header__item">			  
			    <? if ( !empty($arResult["arUser"]["PERSONAL_PHONE"]) ) { ?>
					<div class="lc-header__phone-title"><?=GetMessage("YOUR_PHONE_NUMBER");?></div>
					
					<a class="lc-header__phone-link" href="tel:<?=$arResult["arUser"]["PERSONAL_PHONE"];?>">
						<?=$arResult["arUser"]["PERSONAL_PHONE"];?>
					</a>
				<? } ?> 
              </div>
						  
              <div class="lc-header__email lc-header__item">			  
				  <span class="lc-header__email-title"><?=GetMessage("YOUR_EMAIL");?>
					  <div class="lc-header__email-info"></div>
					  <div class="lc-header__email-tooltip">
						<div class="tooltip">
						  <p class="tooltip__text"><?=GetMessage("YOUR_EMAIL_TEXT");?></p>
						</div>
					  </div>
				  </span>
				  
				  <a class="lc-header__email-link" href="mailto:<?=$arResult["arUser"]["EMAIL"];?>">
					<?=$arResult["arUser"]["EMAIL"];?>
				  </a>				  
			  </div>		  				  
              <div class="lc-header__change-email lc-header__item">
                <button class="button lc-header__change-email__link js-popup-open" type="button" data-popup="changeMail">		<?=GetMessage("CHANGE_EMAIL");?>
				</button>
              </div>
            </div>
          </div>
		  
		  
          <div class="lc-tabs">	 
            <div class="lc-tabs__header">
              <div class="lc-tabs__title"><?=GetMessage("PARTICIPATION_SWEEPSTAKES");?></div>
            </div>		
            <div class="lc-tabs__stage-header">			
				<? $i = 1; foreach ( $arResult["STAGES"] as $item ) { ?>
					<? switch ($i) {
						case 1: $prefix = "first"; break;
						case 2: $prefix = "second"; break;
						case 3: $prefix = "third"; break;
						case 4: $prefix = "four"; break;
					} ?>
					
					<button class="button lc-tabs__stage lc-tabs__stage--<?=$prefix?> <? if ($item["ACTIVE"] == "Y") { ?>active<? } ?>" type="button" data-id="<?=$i?>" <? if ($item["ACTIVE"] == "R") { ?>disabled<? } ?>></button>
					
					<? ++$i; ?>				
				<? } ?>
            </div>
          </div>
		  
		  
          <div class="lc-works">	  
		  
            <div class="lc-works__header">			
				<? foreach ( $arResult["STAGES"] as $item ) { ?>				
					<? switch ($item["ACTIVE"]) 
					{
						case "Y": $status = "Этап активен"; break;
						case "N": $status = "Этап завершен"; break;
						case "R": $status = "Этап закрыт"; break;
					} ?>			
					<div class="lc-works__status <? if ($item["ACTIVE"] == "Y") { ?>active<? } ?>">
						<?=$status;?>
					</div>					
				<? } ?>			  
            </div>			
			
            <div class="lc-works__content">
              <div class="lc-works__descr"><?=GetMessage("COUNT_PHOTO");?></div>
              
			  <? foreach ($arResult["STAGES"] as $stages) { ?> 
			  
			  	<div class="lc-works__body <? if ($stages["ACTIVE"] == "Y") { ?>active<? } ?>">
					<ul class="lc-works__list owl-carousel owl-theme" id="lc-works__list1">
			  
					<? foreach ($stages["PHOTO"] as $key => $photo) { ?> 						 
						  						  
						<li class="lc-works__item">
							<div class="lc-works__item-content">

							  <? if ( !empty($photo["DETAIL_PICTURE"]) ) { ?>
								<div class="lc-works__item-img <?if ($stages["ACTIVE"] == "N") {?>no-active<?}?>" style="background: url(<?=$photo["DETAIL_PICTURE"]?>) no-repeat 50% 50%; background-size: cover;"></div>
							  <? } ?>
							  
							  <? switch ($photo["PROPERTY_MODERATION_STATUS_VALUE"]) {
								  
								case "participates_drawing": ?>
								    <div class="lc-works__item-info lc-works__item-info--green">
									    <?=$photo["MODERATION_STATUS"]?>
								    </div>								
								<? break; 

								case "under_moderation": ?>
								    <div class="lc-works__item-info lc-works__item-info--blue">
										<?=$photo["MODERATION_STATUS"]?>
									</div>								
								<? break;  
								
								case "moderation_error": ?>
								    <div class="lc-works__item-info lc-works__item-info--blue">
										<?=$photo["MODERATION_STATUS"]?>
									</div>
									<div class="lc-works__tooltip lc-works__tooltip--file" style="bottom: -103px;">
										<div class="tooltip">
											<p class="tooltip__text">
												<a class="tooltip__link" href="<?=$arResult["PRAVILA_KONKURSA"];?>" target="_blank">
													<?=$photo["REASON"]?>
												</a>
											</p>
										</div>
									</div>									
								<? break;  

								default: ?>
								
									<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "preview",
									   array(
										  "INPUT_NAME"       => "FILE_UPLOAD".$key,
										  "MULTIPLE"         => "N",									  
										  "MODULE_ID"        => "main",
										  "MAX_FILE_SIZE"    => "",
										  "ALLOW_UPLOAD"     => "A", 
										  "ALLOW_UPLOAD_EXT" => "",
										  "INPUT_CAPTION"    => "Загрузите фото",
										  "INPUT_VALUE"      => $_POST["FILE_UPLOAD".$key],
										  "PHOTO_ID"         => $photo["ID"],
										  "BTN_ACTIVE"		 => $stages["ACTIVE"],
										  "PRAVILA_KONKURSA" => $arResult["PRAVILA_KONKURSA"]
									   ),
									   false
									);?>
								  
								<? break;
								  
							  } ?>
							  
							</div>
						</li>
					<? } ?>
					</ul>
				</div>
			  <? } ?>		  
            </div>
          </div>
		  
	<? if ($arResult["arUser"]["UF_WINNER"]) { ?>	  
          <div class="lc-winners">
            <div class="lc-winners__decor"></div>
            <div class="lc-winners__header">
              <div class="lc-winners__title">Поздравляем!</div>
              <div class="lc-winners__subtitle">Вы выиграли 			  
				<? if ( !empty($arResult["arUser"]["UF_PRIZE"]) ) { ?>
					<?=$arResult["arUser"]["UF_PRIZE"];?>
				<? } ?>!
			  </div>
            </div>
            <div class="lc-winners__content">
              <div class="lc-winners__descr">Чтобы мы могли направить вам приз, пожалуйста, заполните форму.<br/> Призы будут рассылаться 
			  <? if ( !empty($arResult["arUser"]["UF_ISSUANCE"]) ) { ?>
					<?=$arResult["arUser"]["UF_ISSUANCE"];?>
			  <? } ?>.</div>
			  
			  <a class="lc-winners__btn button button--blue" href="/promo/winners-form/" title="Анкета победителя">Заполнить форму!</a>
			  
              <div class="lc-winners__rules">			  
				  <a class="lc-winners__rules-link" href="<?=GetDocument("spisok-pobediteley");?>" download target="_blank">Победители.pdf</a>				  
				  <a class="lc-winners__rules-link" href="<?=$arResult["PRAVILA_KONKURSA"];?>" download target="__blank">Правила конкурса.pdf</a>			  
			  </div>
            </div>
            <div class="lc-winners__info">Организатор конкурса оставляет за собой право изменения количества призов и состава победителей.</div>
          </div>
	<? } else { ?><br><br><br><br><br><br><? } ?>
        </div>
      </div>
	  
	  
<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
</script>