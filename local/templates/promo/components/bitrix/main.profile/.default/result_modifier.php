<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

$arResult["PRAVILA_KONKURSA"] = GetDocument("pravila-konkursa");

//////////////////////////////////////////////////////////////////////////////

use Bitrix\Main\Loader;	
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;	

Loader::includeModule("highloadblock"); 
	
$hlblock = HL\HighloadBlockTable::getById(8)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();

$allValuesStages = [];

$rsData = $entity_data_class::getList(["select" => ["UF_XML_ID", "UF_NAME"]]);

while($arData = $rsData->Fetch()) 
{
	$allValuesStages[$arData["UF_XML_ID"]] = $arData["UF_NAME"];
} 

//////////////////////////////////////////////////////////////////////////////

$hlblock = HL\HighloadBlockTable::getById(9)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();

$allValuesRejection = [];

$rsData = $entity_data_class::getList(["select" => ["UF_XML_ID", "UF_NAME"]]);

while($arData = $rsData->Fetch()) 
{
	$allValuesRejection[$arData["UF_XML_ID"]] = $arData["UF_NAME"];
} 

//////////////////////////////////////////////////////////////////////////////

$arResult["STAGES"] = [];

$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(STAGES_IBLOCK_ID);

$obStages = $entity::getList(array(
	"select" => array("ID", "NAME", "UF_ACTIVE_TO", "UF_ACTIVE_FROM"),
	"filter" => array("IBLOCK_ID" => STAGES_IBLOCK_ID, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_SECTION_ID" => false)
));

$obDateOnline = DateTime::createFromPhp(new \DateTime());

$arStageActive = [];

global $USER;

while($arStage = $obStages->fetch())
{	
	$delta_date = strtotime($arStage["UF_ACTIVE_FROM"]) - strtotime($obDateOnline);
		
	$active = "R";
		
	if ($delta_date <= 0)
		$arStageActive[$arStage["ID"]] = $delta_date; $active = "N";
		
	$arResult["STAGES"][$arStage["ID"]] = [	
		"ID"   		     => $arStage["ID"],
		"NAME" 		     => $arStage["NAME"],
		"UF_ACTIVE_TO"   => $arStage["UF_ACTIVE_TO"],
		"UF_ACTIVE_FROM" => $arStage["UF_ACTIVE_FROM"],
		"STAGE_ID"       => $arStage["ID"],
		"ACTIVE"         => $active
	];	
}

$keyStageActive = array_search(max($arStageActive), $arStageActive);

$arResult["STAGES"][$keyStageActive]["ACTIVE"] = "Y"; 

global $USER;

$obPhotos = CIBlockElement::GetList([], ["IBLOCK_ID" => STAGES_IBLOCK_ID, "PROPERTY_USER_ID" => $USER->GetId()], false, false, ["ID", "IBLOCK_SECTION_ID", "NAME", "DETAIL_PICTURE", "PROPERTY_MODERATION_STATUS", "PROPERTY_REASON"]);

while ($arPhoto = $obPhotos->fetch())
{	
	$arPhoto["REASON"] = $allValuesRejection[$arPhoto["PROPERTY_REASON_VALUE"]];

	$arPhoto["MODERATION_STATUS"] = $allValuesStages[$arPhoto["PROPERTY_MODERATION_STATUS_VALUE"]];
	
	$arPhoto["DETAIL_PICTURE"] = CFile::GetPath($arPhoto["DETAIL_PICTURE"]);
	
	$arResult["STAGES"][$arPhoto["IBLOCK_SECTION_ID"]]["PHOTO"][$arPhoto["ID"]] = $arPhoto;
}

$this->__component->setResultCacheKeys(array("STAGES", "PRAVILA_KONKURSA"));

//////////////////////////////////////////////////////////////////////////////

?>