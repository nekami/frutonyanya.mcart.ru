<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?

if ( !empty($_POST["FILE_ID"]) && !empty($_POST["ELEMENT_ID"]) )
{
	CModule::IncludeModule('iblock');

	$el = new CIBlockElement;
	
	$element_id = $_POST["ELEMENT_ID"];

	$res = $el->Update($element_id, ["DETAIL_PICTURE" => CFile::MakeFileArray($_POST["FILE_ID"])]);
	
	CIBlockElement::SetPropertyValuesEx($element_id, false, array("MODERATION_STATUS" => "under_moderation"));
	
	echo json_encode(["res" => $res]);
}

?>