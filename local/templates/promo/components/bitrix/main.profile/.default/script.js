$(function() 
{
	$('input[type="file"]').click(function()
	{
		$(this).parent('.lc-works__form-label').next('.lc-works__tooltip.lc-works__tooltip--file').hide();
	});	
	
	// загрузка фотографий в элементы инфоблока
	
	BX.addCustomEvent("onAddFile", BX.delegate(function(params1, params2, params3, params4)
	{			
		BX.element_id = Number(params2.params.idPhoto);
		
		BX.file_id    = Number(params1);	
		
		$.ajax({
			url: '/local/templates/promo/components/bitrix/main.profile/.default/ajax.php',
			data: {"ELEMENT_ID": BX.element_id, "FILE_ID": BX.file_id},
			method: 'POST',
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
				console.log(data);
			
			},
			error: function(data)
			{
				console.log("Ошибка ajax-запроса");
			}
		});
		
	}, this));	
	
	
}); 

function removeElement(arr, sElement)
{
	var tmp = new Array();
	for (var i = 0; i<arr.length; i++) if (arr[i] != sElement) tmp[tmp.length] = arr[i];
	arr=null;
	arr=new Array();
	for (var i = 0; i<tmp.length; i++) arr[i] = tmp[i];
	tmp = null;
	return arr;
}

function SectionClick(id)
{
	var div = document.getElementById('user_div_'+id);
	if (div.className == "profile-block-hidden")
	{
		opened_sections[opened_sections.length]=id;
	}
	else
	{
		opened_sections = removeElement(opened_sections, id);
	}

	document.cookie = cookie_prefix + "_user_profile_open=" + opened_sections.join(",") + "; expires=Thu, 31 Dec 2020 23:59:59 GMT; path=/;";
	div.className = div.className == 'profile-block-hidden' ? 'profile-block-shown' : 'profile-block-hidden';
}