<?  if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('<?=SITE_KEY?>').then(function(token) 
		{
			document.getElementById('g-recaptcha-response_2').value=token;
		});
	});
</script>

<?
$replacer = function ($matches) use ($APPLICATION) 
{
	$matches = explode("-", $matches[1]);
	
	ob_start();

	$APPLICATION->IncludeComponent(
		"bitrix:main.calendar",
		"",
		Array(
			"FORM_NAME" => "regform",
			"HIDE_TIMEBAR" => "Y",
			"INPUT_ADDITIONAL_ATTR" => "placeholder=\"Дата\"",
			"INPUT_NAME" => $matches[2],
			"INPUT_NAME_FINISH" => "",
			"INPUT_VALUE" => "",
			"INPUT_VALUE_FINISH" => "",
			"SHOW_INPUT" => "Y",
			"SHOW_TIME" => "N"
		)
	);
	
	return ob_get_clean();
};
	
echo preg_replace_callback(
	"|CALENDAR(.*)|".BX_UTF_PCRE_MODIFIER,
	$replacer,
	$arResult["CACHED_TPL"]
);
?>