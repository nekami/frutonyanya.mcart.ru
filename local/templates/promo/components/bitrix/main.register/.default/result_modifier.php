<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["AUTH_SERVICES"] = false;

if (CModule::IncludeModule("socialservices")) 
{
    $oAuthManager = new CSocServAuthManager();
	
    $arServices = $oAuthManager->GetActiveAuthServices($arResult);
	
    if(!empty($arServices)) $arResult["AUTH_SERVICES"] = $arServices;
	
	$this->__component->setResultCacheKeys(array("AUTH_SERVICES"));
}
?>