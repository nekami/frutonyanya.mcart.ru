$(function() 
{	
	$(document).on('click', '.js-removeChildBtn', function() 
	{
		$(this).parents(".regForm__row").remove();
	});
	
	// размножение полей с детьми
	$(document).on('click', '.js-addChildBtn', function() 
	{
		var last_child = $(".js-addChild-form .regForm__row:last-child")[0].attributes[1].value;
		
		var id = Number(last_child.split('_')[1]) + 1;
		
	    var str = $('<div class="regForm__row"  data-id="CHILD_'+id+'">'
                + '<label class="regForm__label regForm__label--small regForm__margin-right js-childSex">'
                + '<div class="regForm__input-name">Пол</div>'
                + '<div class="regForm__input-wpap"><span class="regForm__input-icon regForm__input-icon--drop"></span>'
                + '<input class="regForm__input" type="text" name="regFormChildSex_0" id="regFormChildSex_0" value="1" readonly="readonly" hidden="hidden">'
                + '<div class="regForm__input regForm__input--pseudo">Сын</div>'
                + '<ul class="regForm__select-list">'
                + '<li class="regForm__select-item" data-type="1">Сын</li>'
                + '<li class="regForm__select-item" data-type="2">Дочь</li>'
                + '</ul>'
                + '</div>'
                + '</label>'
                + '<label class="regForm__label regForm__label--full-medium regForm__margin-right js-childName">'
                + '<div class="regForm__input-name">Имя</div>'
                + '<div class="regForm__input-wpap">'
                + '<input class="regForm__input" type="text" name="regFormChildName_0" id="regFormChildName_0" placeholder="Имя" maxlength="30">'
                + '</div>'
                + '</label>'
                + '<label class="regForm__label regForm__label--medium js-childDate">'
                + '<div class="regForm__input-name">Дата рождения</div>'
                + '<div class="regForm__input-wpap">'				
                + '<button class="button regForm__input-clear js-removeChildBtn" type="button"></button>'
				//+ 'CALENDAR-selectChildren-CHILD_DATE['+id+']'				
				+ '<input class="regForm__input" type="text" id="CHILD_DATE['+id+']" name="CHILD_DATE['+id+']" value="" placeholder="Дата">'
				+ '<img src="/bitrix/js/main/core/images/calendar-icon.gif" alt="Выбрать дату в календаре" class="calendar-icon" onclick="BX.calendar({node:this, field:\'CHILD_DATE['+id+']\', form: \'regform\', bTime: false, currentTime: \'1597770183\', bHideTime: true});" onmouseover=\"BX.addClass(this, \'calendar-icon-hover\');\" onmouseout=\"BX.removeClass(this, \'calendar-icon-hover\');\" border=\"0\">'
                + '</div>'
                + '</label>'
                + '</div>');
		
		$('.js-addChild-form').append(str);
	});
	
	ClearErrorText();
	
	// регистрация пользователя
	$('#regSubmit').click(function(event)
	{
		event.preventDefault();	
		
		var fio                = $('input[name="FIO"]').val();
		var email              = $('input[name="EMAIL"]').val();
		var phone              = $('input[name="PERSONAL_MOBILE"]').val();
		var city               = $('input[name="PERSONAL_CITY"]').val();
		var pass               = $('input[name="PASSWORD"]').val();
		var confirm_pass       = $('input[name="CONFIRM_PASSWORD"]').val();
		var сhild_bearing_date = $('input[name="СHILD_BEARING_DATE"]').val();
		var sessid             = $('input[name="sessid"]').val();
		var recaptcha          = $('input[name="g-recaptcha-response"]').val();
		
		var sex = [];
		
		$('.regForm__input--pseudo').each(function(index, value)
		{			
			sex.push($(value).text());
		});		
				
		var child_name = [];
		
		$.each($('input[name="CHILD_NAME[]"]'), function(code, elem) 
		{	
			child_name.push($(this).val());
		});
				
		var uf_children = [];
		
		for (let i = 0; i<child_name.length; i++)
		{
			uf_children.push(sex[i]+' '+child_name[i]+' '+$('input[name="CHILD_DATE['+i+']"]').val());
		}
		
		if ( !email || !pass || !confirm_pass || !fio || ($('input[name="AGREE"]').val() == "N") )
		{				
			if (!email)
				$('input[name="EMAIL"]').addClass("errortext").next('div.error_text').show();
				
			if (!pass) 
				$('input[name="PASSWORD"]').addClass("errortext").next('div.error_text').show(); 
				
			if (!confirm_pass)
				$('input[name="CONFIRM_PASSWORD"]').addClass("errortext").next('div.error_text').show();
				
			if (!fio) 
				$('input[name="FIO"]').addClass("errortext").next('div.error_text').show();
				
			if ($('input[name="AGREE"]').val() == "N") 
				$('input[name="AGREE"]').addClass("errortext").next('i').next('div.error_text').show();	
		} 
		else 
		{	
			$.ajax({
				url: '/local/templates/promo/components/bitrix/main.register/.default/ajax.php',
				data: {
					'FIO': fio,					  
				    'EMAIL': email, 
				    'PASSWORD': pass, 
					'CONFIRM_PASSWORD': confirm_pass,
					'PERSONAL_MOBILE': phone,					
					'PERSONAL_CITY': city,
					'UF_CHILDREN': uf_children,
					'СHILD_BEARING_DATE': сhild_bearing_date,
					'sessid': sessid,
					'recaptcha': recaptcha
				},
				method: 'POST',
				type: 'POST',
				dataType: 'json',
				success: function(data)
				{
					if (data.error) { 
						$('.error_registration').append(data.error);
					}				
					
					if (data.success) {
						$('[data-popup="forgotpasswd"] .popup__descr').html('На Ваш e-mail отправлено письмо. Перейдите по ссылке из этого письма, чтобы подтвердить e-mail и завершить процедуру регистрации.');
						$('[data-popup="forgotpasswd"]').addClass('active');		
						$('.page__wrapper.filter').addClass('filter');					
						$('.page.scroll').addClass('scroll');
					}				
				},         
				error:  function(data){
					console.log('Ошибка ajax-скрипта');
				}
			});
		}
	});	
	
	$(document).on('click', '.js-close-popup', function() 
	{
		$(location).attr('href', '/promo/');
	});
	
	$(document).on('click', 'input[name="AGREE"]', function() 
	{
		if ($(this).val() == "N") $(this).val("Y");
		else $(this).val("N");
	});		
	
}); 