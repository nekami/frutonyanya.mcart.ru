<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<? 
if ($_POST['g-recaptcha-response'])
{		
	/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
	$Return = getCaptcha($_POST['g-recaptcha-response']);

	$reCaptchaError = false;
			
	/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
	if ($Return->success != true && $Return->score <= 0.5)
	{
		echo json_encode(["error" => "Письма от роботов не принимаются."]);
		die;
	}			
}

if (!empty($_POST['EMAIL']) && !empty($_POST['PASSWORD']) && !empty($_POST['CONFIRM_PASSWORD']) && !empty($_POST['FIO'])) 
{		
	CModule::IncludeModule('main');
	CModule::IncludeModule('iblock');	
	
	$PASSWORD           = trim($_POST['PASSWORD']);
	$CONFIRM_PASSWORD   = trim($_POST['CONFIRM_PASSWORD']);
	
	if ( $PASSWORD != $CONFIRM_PASSWORD ) 
	{
		echo json_encode(['error' => "Поля 'Пароль' и 'Подтверждение пароля' должны совпадать."]);
		die;
	}
	
	$LOGIN = trim($_POST['EMAIL']);	
		
	// проверка на наличие такого же логина или телефона или e-mail	
	$obUser = \Bitrix\Main\UserTable::getList(array(
		'select' => ['ID'],
		'filter' => ['ACTIVE' => 'Y', ['LOGIC' => 'OR','LOGIN' => $LOGIN, 'EMAIL' => $LOGIN]]
	));
	
	if ($arUser = $obUser->fetch()) 
	{
		if (!empty($arUser["ID"])) 
		{ 
			echo json_encode(['error' => 'Такой пользователь уже существует']); 
			die; 
		}
	}	

	$FIO = "";	
	if ( !empty($_POST['FIO']) )
		$FIO = trim($_POST['FIO']);
	
	$PHONE = "";
	if ( !empty($_POST['PERSONAL_MOBILE']) )
		$PHONE = trim($_POST['PERSONAL_MOBILE']);

	$PERSONAL_CITY = "";
	if ( !empty($_POST['PERSONAL_CITY']) )
		$PERSONAL_CITY = trim($_POST['PERSONAL_CITY']);
	
	$UF_CHILDREN = "";
	if ( !empty($_POST['UF_CHILDREN']) )
		$UF_CHILDREN = $_POST['UF_CHILDREN'];		
	
	$UF_PREGNANCY = "";
	if ( !empty($_POST['СHILD_BEARING_DATE']) )
		$UF_PREGNANCY = trim($_POST['СHILD_BEARING_DATE']);
	
	// добавление пользователя
	$user = new CUser;	
	
	$arFields = Array(
	  "ACTIVE"              => "Y",	
	  "EMAIL"               => $LOGIN,
	  "LOGIN"               => $LOGIN,  	  
	  "PASSWORD"            => $PASSWORD,
	  "CONFIRM_PASSWORD"    => $CONFIRM_PASSWORD,
	  "NAME"                => $FIO,
	  "PERSONAL_MOBILE"     => $PHONE,
	  "PERSONAL_CITY"       => $PERSONAL_CITY,
	  "UF_CHILDREN"         => $UF_CHILDREN,
	  "UF_PREGNANCY"        => $UF_PREGNANCY,
	  "LID"                 => "s2",
	  "GROUP_ID"            => [3, 4, GROUP_CONSULTANTS]
	);
	
	$IdUser = $user->Add($arFields);
		
	if (!empty($IdUser)) 
	{
		$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(STAGES_IBLOCK_ID);
		
		$obStages = $entity::getList(array(
		   "select" => array("ID"),
		   "filter" => array("IBLOCK_ID" => STAGES_IBLOCK_ID, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_SECTION_ID" => false)
		));
				
		$el = new CIBlockElement;	
		
		while($arStage = $obStages->fetch())
		{		
			for ($j=1; $j<4; ++$j)
			{
				$arLoadProductArray = 
				[
					  "IBLOCK_SECTION_ID" => $arStage["ID"],
					  "IBLOCK_ID"         => STAGES_IBLOCK_ID,			  
					  "NAME"              => "Фотография ".$arStage["ID"].".".$IdUser.".".$j,
					  "CODE"              => "fotografiya-".$arStage["ID"]."-".$IdUser."-".$j,
					  "ACTIVE"            => "Y",
					  "PROPERTY_VALUES"   => [212 => $IdUser, 214 => "/bitrix/admin/user_edit.php?lang=ru&ID=".$IdUser, 215 =>$PHONE]
				];	
					
				$id = $el->Add($arLoadProductArray);
					
				if(!$id) {
					echo json_encode(['error' => $el->LAST_ERROR]); 
					die; 
				}
			}
		}
		
		CEvent::Send("NEW_USER", ["s1", "s2"], ["USER_ID" => $IdUser, "EMAIL" => $LOGIN, "NAME" => $FIO, "CONFIRM_CODE" => ""], "N", 1);
	}
	
	// авторизация пользователя	
	// $arAuthResult = $user->Login($LOGIN, $PASSWORD, "Y");
	// $APPLICATION->arAuthResult = $arAuthResult;
	//echo json_encode(['success' => $APPLICATION->arAuthResult]);
	
	echo json_encode(['success' => true]);
}

?>