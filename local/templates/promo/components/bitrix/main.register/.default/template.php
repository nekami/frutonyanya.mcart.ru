<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<? if($arResult["SHOW_SMS_FIELD"] == true) CJSCore::Init('phone_auth'); 
ob_start();
?>

	  
      <div class="reg">
        <div class="reg__bg"></div>
        <div class="reg__decor1"></div>
        <div class="reg__decor2"></div>
        <div class="container reg__container">
		
		
		<? if($USER->IsAuthorized()) { ?>

			<p><?=GetMessage("MAIN_REGISTER_AUTH")?></p>

		<? } else { ?>
		
          <div class="reg__header">
            <div class="reg__header-col reg__header-col--first">
              <div class="reg__title">Регистрация</div>
              <div class="reg__text">С указанными регистрационными данными Вы сможете авторизовываться на <a class="reg__link" href="https://frutonyanya.ru" target="_blank">frutonyanya.ru</a></div>
            </div>
            <div class="reg__header-col reg__header-col--second">
			
			<? if ($arResult["AUTH_SERVICES"] && COption::GetOptionString("main", "allow_socserv_authorization", "Y") != "N") { ?>
				<div class="reg__social">
					<div class="reg__social-title">Войти через <br/>социальные сети:</div>
					<ul class="regSocials__list">                         
						<? foreach($arResult["AUTH_SERVICES"] AS $service) { ?>					
							<? 
							$prefix = '';
							switch ( strtolower($service["ID"]) ) {
								case 'vkontakte': $prefix = "vk"; break;
								case 'facebook': $prefix = "fb"; break;
								case 'odnoklassniki': $prefix = "ok"; break;
							} ?>
							  
							<li class="regSocials__item"><a  class="regSocials__link" title="<?=$service["ID"]?>" href="javascript:void(0)" onclick="$('#bx_auth_serv_form<?=$service["ID"]?> a').eq(0).click();">
												
								<div class="regSocials__icon regSocials__icon--<?=$prefix?>"></div>
											
							</a></li>							
						<? } ?>					
					</ul>
				</div>

				<div style="display:none">
					<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
						array(
							"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
							"AUTH_URL" => $arResult["AUTH_URL"],
							"POST" => $arResult["POST"],
							"POPUP" => "N",
							"SUFFIX" => "form",
						),
						$component,
						array("HIDE_ICONS" => "Y")
					); ?>
				</div>
		 <? } ?>
			  
              <div class="reg__auth">
                <div class="reg__text"><a class="reg__auth-link js-popup-open" href="#" data-popup="auth">Войдите</a><span>, если у вас есть учетная запись на сайте</span></div>
              </div>
			  
            </div>
        </div>
		  
        <div class="reg__form">	

			<? if (count($arResult["ERRORS"]) > 0) {
						
				foreach ($arResult["ERRORS"] as $key => $error)
					if (intval($key) == 0 && $key !== 0) 
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

				ShowError(implode("<br />", $arResult["ERRORS"]));

			}  elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y")  { ?>
					
					<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
						
			<? } ?>
		  
            <form class="regForm" action="<?=POST_FORM_ACTION_URI?>" method="post" id="regForm" name="regform" enctype="multipart/form-data">
			
				<? if($arResult["BACKURL"] <> '') { ?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<? } ?>
			
              <div class="regForm__addData">
                <div class="regForm__row">
                  <label class="regForm__label regForm__margin-right"> 
                    <div class="regForm__input-name">Имя *</div>
                    <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
					  <input class="regForm__input" type="text" name="FIO" required="required" placeholder="Ваше имя" value="<?=$arResult['VALUES']['NAME']?>"/>
					  <div class="error_text">Поле обязательно для заполнения</div>
                    </div>
                  </label>
                  <label class="regForm__label">
                    <div class="regForm__input-name">E-mail *
                      <div class="regForm__label-info"></div>
                      <div class="regForm__label-info__tooltip">
                        <div class="tooltip">
                          <p class="tooltip__text">Ваш e-mail понадобится для того, чтобы получить приз в случае выигрыша. Убедитесь, что указанный адрес является действующим.</p>
                        </div>
                      </div>
                    </div>
                    <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
                      <input class="regForm__input" type="email" name="EMAIL" required="required" placeholder="Ваш e-mail" value="<?=$arResult['VALUES']['EMAIL']?>"/>
					  <div class="error_text">Поле обязательно для заполнения</div>
                    </div>
                  </label>
                </div>
                <div class="regForm__row">
                  <label class="regForm__label regForm__margin-right"> 
                    <div class="regForm__input-name">Телефон</div>
                    <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>					  
					  <input class="regForm__input" type="text" name="PERSONAL_MOBILE" required="required" placeholder="+7 ____ - ____ - ______" value="<?=$arResult['VALUES']['PERSONAL_MOBILE']?>"/>
                    </div>
                  </label>
				   
					  
					    <?$APPLICATION->IncludeComponent(
							"bitrix:sale.location.selector.search",
							"",
							Array(
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
								"CODE" => "",
								"FILTER_BY_SITE" => "Y",
								"FILTER_SITE_ID" => "s2",
								"ID" => "",
								"INITIALIZE_BY_GLOBAL_EVENT" => "",
								"INPUT_NAME" => "PERSONAL_CITY",
								"JS_CALLBACK" => "",
								"JS_CONTROL_GLOBAL_ID" => "",
								"PROVIDE_LINK_BY" => "code",
								"SHOW_DEFAULT_LOCATIONS" => "N",
								"SUPPRESS_ERRORS" => "N"
							)
						);?>

                </div>
				
				
                <div class="regForm__row">
                  <label class="regForm__label regForm__margin-right"> 
                    <div class="regForm__input-name">Пароль *</div>
                    <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>					  
                      <input class="regForm__input" type="password" name="PASSWORD" required="required" placeholder="" value="<?=$arResult['VALUES']['PASSWORD']?>"/>
					  <div class="error_text">Поле обязательно для заполнения</div>
                    </div>
                  </label>
                  <label class="regForm__label">
                    <div class="regForm__input-name">Подтверждение пароля *</div>
                    <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
                      <input class="regForm__input" type="password" name="CONFIRM_PASSWORD" required="required" placeholder="" value="<?=$arResult['VALUES']['CONFIRM_PASSWORD']?>"/>
					  <div class="error_text">Поле обязательно для заполнения</div>
                    </div>
                  </label>
                </div>
              </div>
			  
<!--------------------------------------------------------------------------->
			  
            <div class="regForm__addExtraData">			
				<div class="regForm__addChild">
                  <div class="js-addChildWrap">
                    <label class="regForm__label-checkbox js-addChildCheck">
                      <input class="regForm__checkbox" type="checkbox" name="selectChildren" id="selectChildren"/><i></i><span class="regForm__checkbox-name">Есть дети</span>
                    </label>
                    <div class="regForm__body js-addChildRes">
                      <div class="js-addChildBlocks">
                        <div class="regForm__body-form js-addChildBlock" data-count="0">
                          <div class="regForm__addChild-form js-addChild-form">
                            <div class="regForm__row" data-id="CHILD_0">
                              <label class="regForm__label regForm__label--small regForm__margin-right js-childSex">
                                <div class="regForm__input-name">Пол</div>
                                <div class="regForm__input-wpap"><span class="regForm__input-icon regForm__input-icon--drop"></span>
                                  <input class="regForm__input" type="text" name="regFormChildSex_0" id="regFormChildSex_0" value="1" readonly="readonly" hidden="hidden"/>
                                  <div class="regForm__input regForm__input--pseudo">Сын</div>
                                  <ul class="regForm__select-list">
                                    <li class="regForm__select-item" data-type="1">Сын</li>
                                    <li class="regForm__select-item" data-type="2">Дочь</li>
                                  </ul>
                                </div>
                              </label>
                              <label class="regForm__label regForm__label--full-medium regForm__margin-right js-childName">
                                <div class="regForm__input-name">Имя</div>
                                <div class="regForm__input-wpap">
                                  <input class="regForm__input" type="text" name="regFormChildName_0" id="regFormChildName_0" placeholder="Имя" maxlength="30"/>
                                </div>
                              </label>
                              <label class="regForm__label regForm__label--medium js-childDate">
                                <div class="regForm__input-name">Дата рождения</div>
                                <div class="regForm__input-wpap">
                                  <button class="button regForm__input-clear js-removeChildBtn" type="button"></button>
                                  CALENDAR-selectChildren-CHILD_DATE[0]
                                </div>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button class="regForm__addChild-btn button button--noBg js-addChildBtn" type="button">Добавить ребёнка</button>
                    </div>
                  </div>
                </div>
                <div class="regForm__addChildBearing">
                  <label class="regForm__label-checkbox js-addChildBearingCheck">
                    <input class="regForm__checkbox" type="checkbox" name="selectChildBearing" value="no" id="selectChildBearing"/><i></i><span class="regForm__checkbox-name">Беременность</span>
                  </label>
                  <div class="regForm__body">
                    <label class="regForm__label regForm__label--medium">
                      <div class="regForm__input-name">Рождение ребёнка</div>
                      <div class="regForm__input-wpap">
                        <button class="button regForm__input-clear" type="button"></button>
                        CALENDAR-selectChildBearing-СHILD_BEARING_DATE
                      </div>
                    </label>
                  </div>
                </div>				
                <div class="regForm__rules">
                  <div class="regForm__rules-checkbox">
                    <label class="regForm__label-checkbox">
                      <input class="regForm__checkbox" type="checkbox" name="AGREE" id="rulesAccept" value="N"/><i></i>
					  <div class = "error_text" style="position: absolute; top: 35px;">Вы не приняли условия пользовательского соглашения</div>
                    </label>
                  </div>
                  <div class="regForm__rules-descr">
                    <p>Я ознакомился с <a href="<?=GetDocument("polzovatelskoe-soglashenie");?>" download target="__blank">Пользовательским соглашением</a> и даю согласие на обработку моих персональных данных </p>
                  </div>
                </div>
                <p class="regForm__rules-after regForm__rules-descr" style="margin-top: 50px;">* — поля обязательные для заполнения</p>				
				
				<div class="regForm__captcha error_registration" data-name="g-recaptcha-response">
					<input type="hidden" id="g-recaptcha-response_2" name="g-recaptcha-response" value="">				
					<div class="error_text">От роботов письма не принимаются.</div>
				</div>
                		
                <button class="regForm__submit button button--blue" type="submit" id="regSubmit">Зарегистрироваться</button>
            </div>
            </form>
        </div>
		  
		<? } ?>
		  
        </div>
      </div>
	  
<?
$this->__component->SetResultCacheKeys(array("CACHED_TPL"));
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();						
?>
	
<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});	
</script>