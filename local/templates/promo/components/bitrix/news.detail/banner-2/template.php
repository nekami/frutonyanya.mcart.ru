<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

    <div class="play" id="play">
        <div class="play__decor"></div>
        <div class="container play__container">
          <div class="play__round1 play__animate animation" data-start="100" data-direction="up" data-duration="2" data-rotate="data-rotate"></div>
          <div class="play__round2 play__animate animation" data-start="1500" data-direction="up" data-duration="2" data-rotate="data-rotate"></div>
          <div class="play__round3 play__animate animation" data-start="2500" data-direction="up" data-duration="2" data-rotate="data-rotate"></div>
          <div class="play__giraffe play__animate animation" data-start="100" data-direction="up" data-duration="2"></div>
          <div class="play__header">
            <h2 class="play__title"><?=$arResult["NAME"];?></h2>
          </div>
          <div class="play__content play__animate animation" data-start="1500" data-direction="up" data-duration="2">
            <div class="play__desc play__desc-first"><?=$arResult["PREVIEW_TEXT"];?></div>
            <div class="play__desc play__desc-second"><?=$arResult["DETAIL_TEXT"];?></div>
          </div>
        </div>
    </div>