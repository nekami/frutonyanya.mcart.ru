<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
	  
    <div class="welcome" id="welcome">
        <div class="welcome__bg"></div>
        <div class="welcome__lenivec">
          <div class="welcome__lenivec-foot animation__foot"></div>
          <div class="welcome__lenivec-info welcome__animate animation" data-start="500" data-direction="right" data-duration="2" data-position="50"><a class="welcome__info js-scroll-btn" href="#skills">
              <div class="welcome__info-title">Давайте <br/>играть</div>
              <div class="welcome__info-descr">тут столько <br/>идей</div></a></div>
        </div>
        <div class="container welcome__container">
          <div class="welcome__leaves1 animation__rotate-easy"> </div>
          <div class="welcome__leaves2 animation__leftRight"></div>
          <div class="welcome__leaves3 animation__rotate-mid"></div>
          <div class="welcome__pchela welcome__animate animation" data-start="500" data-direction="left" data-duration="2"></div>
          <div class="welcome__content">
            <div class="welcome__cloud"></div>
            <div class="welcome__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/welcome/title.png" alt="Настоящее приключение"/></div>
            <h1 class="welcome__title"><?=$arResult["PREVIEW_TEXT"];?></h1><a class="button button--primary welcome__button js-scroll-btn" href="#action"><?=GetMessage("TAKE_PART");?></a>
          </div>
        </div>
    </div>