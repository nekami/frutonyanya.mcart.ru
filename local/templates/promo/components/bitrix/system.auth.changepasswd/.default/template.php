<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<script>
	BX.LOGIN = '<?=$_GET["USER_LOGIN"]?>';
</script>

<div class="reg lc__reg">
    <div class="container reg__container">
        <div class="popup__form form__page">

<? ShowMessage($arParams["~AUTH_RESULT"]); ?>

<?if($arResult["SHOW_FORM"]):?>

	<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform" class="regForm">

		<?if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<? endif ?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">
		<input type="hidden" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>"/>	
		<input type="hidden" name="USER_CHECKWORD" value="<?=$arResult["USER_CHECKWORD"]?>"/>
		
		<div class="popup__header">
			<div class="popup__title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></div>
		</div>	

		<div class="regForm__row regForm__row--popup">
			<label class="regForm__label">
				<div class="regForm__input-name"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></div>
				<div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
					<input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" class="regForm__input" autocomplete="off" />
					<div class="error_text">Поле обязательно для заполнения</div>
				</div>
			</label>
			<label class="regForm__label">
				<div class="regForm__input-name"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></div>
				<div class="regForm__input-wpap"><span class="regForm__input-icon"></span>						  
					<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="regForm__input" autocomplete="off" />
					<div class="error_text">Поле обязательно для заполнения</div>
				</div>
			</label>
		</div>
					
		<div class="regForm__captcha regForm__captcha--popup" data-name="g-recaptcha-response">
			<input type="hidden" id="g-recaptcha-response_20" name="g-recaptcha-response" value="">				
			<div class="error_text">От роботов письма не принимаются.</div>
		</div>
		
		<div id="bx_chpass_error" class="error_text">Пароли должны совпадать</div>
						
		<div class="regForm__btns">
			<?/*<button class="regForm__auth button button--blue" type="submit" id="authSubmit2">Войти</button>*/?>
			<input type="submit" class="regForm__auth button button--blue" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" id="authSubmit3"/>
			<a class="button regForm__reg" href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a>
		</div>
	</form>

	<p><?=$arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>

<?endif?>

		</div>
	</div>
</div>