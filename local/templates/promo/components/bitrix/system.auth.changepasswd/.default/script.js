$(function() 
{	
	$('input[name="change_pwd"]').click(function(event)
	{
		event.preventDefault();	
		
		var pass            = $('input[name="USER_PASSWORD"]').val();
		var confirm_pass    = $('input[name="USER_CONFIRM_PASSWORD"]').val();

		if (!pass || !confirm_pass || (pass != confirm_pass))
		{
			if (!pass)
				$('input[name="USER_PASSWORD"]').addClass("errortext").next('div.error_text').show(); 
			if (!confirm_pass)
				$('input[name="USER_CONFIRM_PASSWORD"]').addClass("errortext").next('div.error_text').show(); 
			if (pass != confirm_pass)
				$("#bx_chpass_error.error_text").text("Пароли должны совпадать").show();
		} 
		else 
		{	
			$.ajax({
			  url: '/local/templates/promo/components/bitrix/system.auth.changepasswd/.default/ajax.php',
			  data: {'USER_PASSWORD': pass, 'USER_CONFIRM_PASSWORD': confirm_pass, 'LOGIN': BX.LOGIN},
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				  if (data.success)
					  $(location).attr('href', '/promo/profile/');
				  
				  if (data.error)
					$("#bx_chpass_error.error_text").text(data.error).show();
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 