<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if ($_POST['g-recaptcha-response'])
{		
	/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
	$Return = getCaptcha($_POST['g-recaptcha-response']);
			
	/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
	if ($Return->success != true && $Return->score <= 0.5)
	{
		echo json_encode(['error' => 'Письма от роботов не принимаются']);
		die;
	}			
}

if ( !empty($_POST['USER_PASSWORD']) && !empty($_POST['USER_CONFIRM_PASSWORD']) && !empty($_POST['LOGIN'])) 
{	
	CModule::IncludeModule('main');
	
	$password = trim($_POST['USER_PASSWORD']);
	$confirm_password = trim($_POST['USER_CONFIRM_PASSWORD']);	
	$login = trim($_POST['LOGIN']);
	
	if ( $password != $confirm_password ) 
	{
		echo json_encode(['error' => "Поля 'Пароль' и 'Подтверждение пароля' должны совпадать."]);
		die;
	}
	
	$rsUsers = CUser::GetList(($by="ID"), ($order="desc"), ['LOGIN' => $login], ['FIELDS' => ['ID', "NAME"]]);		
	$UserId = '';
	if ($arUser = $rsUsers->Fetch()) 
	{
		$UserId = $arUser['ID'];
	}
	
	if ( !empty($UserId) )
	{		
		$user = new CUser;		
		$resUpdate = $user->Update($UserId, ["PASSWORD" => $password, "CONFIRM_PASSWORD" => $password]);		
		
		if ($resUpdate)
			echo json_encode(['success' => true]);	
		else
			echo json_encode(['error' => "Error update pass."]);
	} 
	else
	{
		echo json_encode(['error' => "Нет пользователя с такием e-mail"]);
	}	
} 	
?>