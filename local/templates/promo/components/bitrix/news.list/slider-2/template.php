<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

	  
      <div class="works" id="works">
        <div class="works__bg"></div>
        <div class="container works__container">
          <div class="works__decor works__decor1 animation" data-start="100" data-direction="left" data-duration="2"></div>
          <div class="works__decor works__decor2 animation" data-start="100" data-direction="right" data-duration="3"></div>
          <div class="works__header">
            <h2 class="works__title"><?=GetMessage("WORK_PARTICIPANTS");?></h2>
          </div>
          <div class="works__content">
            <div class="works__stage-header">			
				<? $i = 1; foreach ( $arResult["STAGES"] as $item ) { ?>
					<? switch ($i) {
						case 1: $prefix = "first"; break;
						case 2: $prefix = "second"; break;
						case 3: $prefix = "third"; break;
						case 4: $prefix = "four"; break;
					} ?>
					
					<button class="button works__stage works__stage--<?=$prefix?> <? if ($item["ACTIVE"] == "Y") { ?>active<? } ?>" type="button" data-id="<?=$i?>" <? if ($item["ACTIVE"] == "R") { ?>disabled<? } ?>></button>
					
					<? ++$i; ?>				
				<? } ?>
            </div>
            <div class="works__stage-body">
				<? $i = 1; foreach ($arResult["STAGES"] as $stages) { ?> 
					<div class="works__stage-content <? if ($stages["ACTIVE"] == "Y") { ?>active<? } ?>">
						<ul class="works__list owl-carousel owl-theme" id="works__list<?=$i?>">
						<? foreach ($stages["PHOTO"] as $key => $photo) { ?>
							<li class="works__item">
							  <? if ( !empty($photo["DETAIL_PICTURE"]) ) { ?>						
								<div class="works__item-img" style="background: url(<?=$photo["DETAIL_PICTURE"]?>) no-repeat 50% 50%; background-size: cover;"></div>
							  <? } ?>
							</li>
						<? } ?>
						</ul>
					<div class="works__counter"></div>
					</div>
					<? ++$i; ?>
				<? } ?>
            </div>			
			<div class="works__buttons">
				<a class="button button--green works__button" href="<?=GetDocument("spisok-pobediteley");?>" download target="_blank"><?=GetMessage("WINNERS");?></a>
				<a class="button button--primary works__button js-scroll-btn" href="#action"><?=GetMessage("PARTICIPATE");?></a>
			</div>
          </div>
        </div>
      </div>