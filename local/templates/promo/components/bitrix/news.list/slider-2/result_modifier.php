<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

//////////////////////////////////////////////////////////////////////////////

use Bitrix\Main\Loader;	
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;	

Loader::includeModule("highloadblock"); 
	
$hlblock = HL\HighloadBlockTable::getById(8)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass();

$allValuesStages = [];

$rsData = $entity_data_class::getList(["select" => ["UF_XML_ID", "UF_NAME"]]);

while($arData = $rsData->Fetch()) 
{
	$allValuesStages[$arData["UF_XML_ID"]] = $arData["UF_NAME"];
} 

//////////////////////////////////////////////////////////////////////////////

$arResult["STAGES"] = [];

$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(STAGES_IBLOCK_ID);

$obStages = $entity::getList(array(
	"select" => array("ID", "NAME", "UF_ACTIVE_TO", "UF_ACTIVE_FROM"),
	"filter" => array("IBLOCK_ID" => STAGES_IBLOCK_ID, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_SECTION_ID" => false)
));

$obDateOnline = DateTime::createFromPhp(new \DateTime());

$arStageActive = [];

$arEquivalenceStages = [];

while($arStage = $obStages->fetch())
{
	$delta_date = strtotime($arStage["UF_ACTIVE_FROM"]) - strtotime($obDateOnline);
		
	$active = "R";
		
	if ($delta_date <= 0)
	{ 
		$arStageActive[$arStage["ID"]] = $delta_date; 
		$active = "N";
	}
	
	$arResult["STAGES"][$arStage["ID"]] = [	
		"ID"   		     => $arStage["ID"],
		"NAME" 		     => $arStage["NAME"],
		"UF_ACTIVE_TO"   => $arStage["UF_ACTIVE_TO"],
		"UF_ACTIVE_FROM" => $arStage["UF_ACTIVE_FROM"],
		"ACTIVE"         => $active
	];
}

$keyStageActive = array_search(max($arStageActive), $arStageActive);

$arResult["STAGES"][$keyStageActive]["ACTIVE"] = "Y"; 

$obPhotos = CIBlockElement::GetList([], ["IBLOCK_ID" => STAGES_IBLOCK_ID, "!=DETAIL_PICTURE" => false, "=PROPERTY_MODERATION_STATUS" => "participates_drawing"], false, false, ["ID", "IBLOCK_SECTION_ID", "NAME", "DETAIL_PICTURE"]);

while ($arPhoto = $obPhotos->fetch())
{
	// установить категорию, ориентируясь на совпадение по категории
	
	$arPhoto["DETAIL_PICTURE"] = CFile::GetPath($arPhoto["DETAIL_PICTURE"]);
	
	$arResult["STAGES"][$arPhoto["IBLOCK_SECTION_ID"]]["PHOTO"][$arPhoto["ID"]] = $arPhoto;
}

$this->__component->setResultCacheKeys(array("STAGES"));

//////////////////////////////////////////////////////////////////////////////

?>