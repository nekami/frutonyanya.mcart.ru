<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

	  
      <div class="action" id="action">
        <div class="action__decor"></div>
        <div class="container action__container">
          <div class="action__content">
            <div class="action__header">
              <h1 class="action__title"><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h1>
            </div>
            <ul class="action__list">
			
			<? $i=1; foreach($arResult["ITEMS"] as $arItem) { ?>
				
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>	

				<? switch ($i) {
					case 1: $color = "green"; break;
					case 2: $color = "blue"; break;
					case 3: $color = "pink"; break;
				} ?>
				
				<li class="action__item">	
					
					<div class="action__img-wrap animation__img">
						<img
							class="action__img"
							src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"						
							alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
							title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						/>
						<div class="action__img-shadow animation__shadow"></div>
					</div>
					
					
					<h3 class="action__item-title action__item-title--<?=$color?>">
						<?=$arItem["NAME"]?>
					</h3>				
					
					<p class="action__desc">
						<?=$arItem["PREVIEW_TEXT"];?>
					</p>				
					
					<? if ( !empty($arItem["PROPERTIES"]["UNITS"]["VALUE"]) ) { ?>				
						<div class="action__info">				
							<b class="action__info-title">Смотрите разделы:</b>					
							<? foreach ($arItem["PROPERTIES"]["UNITS"]["VALUE"] as $key => $value) { ?> 					
								<a class="action__info-link js-scroll-btn" href="#<?=$arItem["PROPERTIES"]["UNITS"]["DESCRIPTION"][$key]?>" title="<?=$value?>">
									<?=$value?>
								</a>					
							<? } ?>					
						</div>
					<? } else { ?> 
						
						<div class="form_foto">
						
							<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "",
							   array(
								  "INPUT_NAME"=>"FILE_UPLOAD",
								  "MULTIPLE"=>"Y",
								  "MODULE_ID"=>"main",
								  "MAX_FILE_SIZE"=>"",
								  "ALLOW_UPLOAD"=>"A", 
								  "ALLOW_UPLOAD_EXT"=>"",
								  "INPUT_CAPTION" => "Загрузить",
								  "INPUT_VALUE" => $_POST['FILE_UPLOAD']
							   ),
							   false
							);?>
							
							<? // на кнопку отправить событие загрузки фотографий ?>
							
							<button type="submit" class="form_foto__button">Отправить</button>
							
						</div>
						
					<? } ?>
				  </li>
			  
				<? ++$i; ?>
			  
			<? } ?>
			  
            </ul>
          </div>
        </div>
      </div>