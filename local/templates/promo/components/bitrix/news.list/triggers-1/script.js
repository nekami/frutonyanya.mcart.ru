$(function() {
	
/******************************************************************/	
	
	// учитывает загрузку одного файла
	
	BX.addCustomEvent("onAddFile", BX.delegate(function(params1, params2, params3, params4)
	{	
		$error = false;
		
		$(".file-input ol li").each( function(code, elem) 
		{
			if (!$(elem).hasClass('saved')) $error = true;
		});
		
		// после загрузки всех фото, сделать видимой кнопку "Отправить"
		
		if (!$error) {
			$('.form_foto__button').show();
		}
		
	}, this));	

/******************************************************************/	

	$('.form_foto__button').click( function() 
	{
		let obPhoto = $(this).parent('.form_foto').find('ol li');
		
		let arIdPhoto = [];
		
		let arNamePhoto = []; 
		
		$.each(obPhoto, function(code, elem) 
		{
			let str = elem.innerHTML;		
			
			let name = str.match(/<a [\D0-9a-zA-Z-.]+>([а-яА-Я0-9a-zA-Z-_ ]+).[\D0-9a-zA-Z-]+<\/a>/);
			
			if (name && name[1])
			{
				arNamePhoto.push(name[1]);
			}

			let result = str.match(/fileID=(\d+)/);
			
			if (result && result[1])
			{
				arIdPhoto.push(result[1]);
			}	
		});
		
		if (arIdPhoto)
		{
			BX.onCustomEvent("GetFileId", [arIdPhoto, arNamePhoto]);
		}
		
	});
	
/******************************************************************/	

}); 