<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<ul class="skills__list owl-carousel owl-theme" id="skills__list1">
              
	<? foreach($arResult["ITEMS"] as $arItem) { ?>
			  
		<li class="skills__item"><?=$arItem["ID"]?>
		
			<div class="skills__item-img" style="background: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>) 50% 50% no-repeat; background-size: cover;"></div>
			
			<h3 class="skills__item-title"><?=$arItem["NAME"]?></h3>		
			
			<a class="skills__item-desc" href="<?=$arItem["PROPERTIES"]["FILE"]["VALUE"]["SRC"]?>">
				<?=$arItem["PROPERTIES"]["FILE"]["VALUE"]["ORIGINAL_NAME"]?>
			</a>
			
		</li>

	<? } ?>
</ul>