$(function() 
{	
	BX.addCustomEvent("GetFileId", BX.delegate(function(arIdPhoto, arNamePhoto)
	{
		$.ajax({
			url: '/local/templates/promo/components/bitrix/news.list/slider-42/ajax.php',
			data: {'arIdPhoto': arIdPhoto, 'original_name': arNamePhoto},
			method: 'POST',
			type: 'POST',
			dataType: 'html',
			success: function(data)
			{
				if (data)
				{
					$('#skills__list2 .owl-stage .owl-item.active:first').before(data);
					
					
					for (let i = 0; i <= arIdPhoto.length; i++)
					{
						$('#skills__list2 .owl-stage .owl-item.active:eq('+i+')').removeClass('active').addClass("cloned");
					}	
					
					
					$('html').animate({
						scrollTop: $('[data-id="skills__list2"]').offset().top - 160 
					}, 700); 

					
					$('.form_foto__button').hide(); // и кнопку "Отправить"
					
					$(".file-input ol li").each( function(code, elem) 
					{
						$(elem).remove();
					});
				}
			},
			error: function(data)
			{
				console.log("Ошибка ajax-запроса");
			}
		});
		
	}, this));
});	