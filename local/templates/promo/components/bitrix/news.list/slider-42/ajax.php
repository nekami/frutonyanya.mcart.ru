<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?	
CModule::IncludeModule('iblock');
$el = new CIBlockElement;
global $USER;
?>

<? if ( !empty($_POST["arIdPhoto"]) && !empty($_POST["original_name"]) ) 
{
	foreach ($_POST["arIdPhoto"] as $key => $item)
	{
		$path = CFile::GetPath($item);
		
		$detail_picture = CFile::MakeFileArray($path);
		
		$name = $_POST["original_name"][$key];
		
		$arLoadProductArray =
		[	  
		  "IBLOCK_SECTION_ID" => 892,
		  "IBLOCK_ID"         => 41,
		  "PROPERTY_VALUES"   => [205 => $USER->GetID()],
		  "NAME"              => $name,
		  "ACTIVE"            => "Y",
		  "DETAIL_PICTURE"    => $detail_picture
		];

		if($PRODUCT_ID = $el->Add($arLoadProductArray)) 
		{
			echo '<div class="owl-item active" style="width: 285px;"><li class="skills__item">'.$item.'<div class="skills__item-img"><img src="'.$path.'" alt="'.$name.'" title="'.$name.'"></div><h3 class="skills__item-title">'.$name.'</h3></li></div>';
		}
	}	
}
?>