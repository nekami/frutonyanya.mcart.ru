<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h2 class="skills__content-title"><?=GetMessage("IDEAS_FROM_MOMS")?></h2>
			
<ul class="skills__list owl-carousel owl-theme" id="skills__list2" data-id="skills__list2">              
				
	<? foreach($arResult["ITEMS"] as $arItem) { ?>
					
		<li class="skills__item"><?=$arItem["ID"]?>
			<div class="skills__item-img" style="background: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>) 50% 50% no-repeat; background-size: cover;"></div>
			<h3 class="skills__item-title"><?=$arItem["NAME"]?></h3>
		</li>
					
	<? } ?>
</ul>

<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
</script>