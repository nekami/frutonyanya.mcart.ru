<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

      <div class="winners" id="winners">
        <div class="winners__bg"></div>
        <div class="container winners__container">
          <div class="winners__decor"></div>
          <div class="winners__pchela winners__animate animation" data-start="100" data-direction="left" data-duration="2"></div>
          <div class="winners__packs animation__img"></div>
          <div class="winners__card">
            <div class="winners__card-img"></div>
            <div class="winners__card-shadow"></div>
          </div>
          <div class="winners__card1"></div>
          <div class="winners__lupa animation__img"></div>
          <div class="winners__header">
            <h2 class="winners__title"><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h2>
          </div>
          <div class="winners__content">
            <div class="winners__stock">
              <div class="winners__desc">
                <div class="winners__desc-top"><?=$arResult["ITEMS"][0]["NAME"]?></div>
                <div class="winners__desc-center"><?=$arResult["ITEMS"][0]["PREVIEW_TEXT"];?></div>
              </div>
            </div>
            <div class="winners__sert">
              <div class="winners__desc">
                <div class="winners__desc-top"><?=$arResult["ITEMS"][1]["NAME"]?></div>
                <div class="winners__desc-center"><?=$arResult["ITEMS"][1]["PREVIEW_TEXT"];?></div>
                <div class="winners__desc-bottom">
                  <div class="winners__desc-bottom__col winners__desc-bottom__col--left">
                    <div class="winners__desc-bottom__bonus winners__desc-bottom__bonus--orange"><?=$arResult["ITEMS"][1]["PROPERTIES"]["UNITS"]["DESCRIPTION"][0]?></div>
                    <div class="winners__desc-bottom__text"><?=$arResult["ITEMS"][1]["PROPERTIES"]["UNITS"]["VALUE"][0]?></div>
                  </div>
                  <div class="winners__desc-bottom__col winners__desc-bottom__col--right">
                    <div class="winners__desc-bottom__bonus winners__desc-bottom__bonus--green"><?=$arResult["ITEMS"][1]["PROPERTIES"]["UNITS"]["DESCRIPTION"][1]?></div>
                    <div class="winners__desc-bottom__text"><?=$arResult["ITEMS"][1]["PROPERTIES"]["UNITS"]["VALUE"][1]?></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="winners__works">
              <div class="winners__desc">
                <div class="winners__desc-top"><?=$arResult["ITEMS"][2]["NAME"]?></div>
                <div class="winners__desc-center"><?=$arResult["ITEMS"][2]["PREVIEW_TEXT"];?></div>
              </div>
            </div>
          </div>
          <div class="winners__bottom">
            <div class="winners__button"><a class="button button--primary js-scroll-btn" href="#action">Участвовать</a></div><a class="winners__rules" href="<?=GetDocument("pravila-konkursa");?>" download target="__blank">полные правила.pdf</a>
          </div>
        </div>
      </div>