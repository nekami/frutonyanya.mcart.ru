<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>


      <div class="workout">
        <div class="workout__decor"></div>
        <div class="container workout__container">
          <div class="workout__header">
            <h2 class="workout__title"><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h2>
          </div>
          <div class="workout__content">
            <ul class="workout__list owl-carousel owl-theme">
			
				<? foreach($arResult["ITEMS"] as $arItem) { ?>
			
				  <li class="workout__item">
				  
					<div class="workout__item-img">
						<img
							src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"						
							alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
							title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
						/>
					</div>
										
					<div class="workout__item-content">
					
					  <h3 class="workout__item-title"><?=$arItem["NAME"]?></h3>
					  
					  <p class="workout__item-desc"><?=$arItem["PREVIEW_TEXT"];?></p>
					  
					  <p class="workout__item-desc"><?=$arItem["DETAIL_TEXT"];?></p>				  
					  
					</div>
				  </li>
				  
				<? } ?>

            </ul>
          </div>
        </div>
      </div>