<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true); ?>
	  
    <div class="product" id="product">
        <div class="product__decor"></div>
        <div class="product__decor1"></div>
        <div class="container product__container">
          <div class="product__header">
            <h2 class="product__title"><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h2>
          </div>
          <ul class="product__list owl-carousel owl-theme">
			<? foreach($arResult["ITEMS"] as $arItem) { ?>
				<li class="product__item">
				  <div class="product__item-wrap">
					<? if ( !empty($arItem["DETAIL_PICTURE"]["SRC"]) ) { ?>
					  <div class="product__img">
						<img src="<?=$arItem["DETAIL_PICTURE"]["SRC"];?>" alt="<?=$arItem["DETAIL_PICTURE"]["ALT"];?>" title="<?=$arItem["DETAIL_PICTURE"]["TITLE"];?>"/>
					  </div>
					<? } ?>
					<div class="product__content">
					  <h3 class="product__content-title"><?=$arItem["NAME"]?></h3>
					  <? if ( !empty($arItem["DETAIL_TEXT"]) ) { ?>
						<div class="product__content-desc"><?=$arItem["DETAIL_TEXT"];?></div>
					  <? } ?>
					</div>
				  </div>
				</li>
			<? } ?>
          </ul>
        </div>
    </div>