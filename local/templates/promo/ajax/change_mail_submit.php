<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?
$strError = ""; 

if ($_POST['g-recaptcha-response'])
{		
	/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
	$Return = getCaptcha($_POST['g-recaptcha-response']);

	$reCaptchaError = false;
			
	/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
	if ($Return->success != true && $Return->score <= 0.5)
	{		
		echo json_encode(['str_error' => "Письма от роботов не принимаются."]);
		die;
	}			
}

if ( !empty($_POST["changeMail"]) && !empty($_POST["profilePassword"]) )
{		
	$password = trim($_POST["profilePassword"]);
	$new_email = trim($_POST["changeMail"]);
	$old_email = trim($_POST["old-email"]);

	CModule::IncludeModule('main');
	
	global $USER;
	
	$userId = $USER->GetID();
	
	if ( !empty(isUserPassword($userId, $password)) )
	{
		$user = new CUser;
		
		$resUpdate = $user->Update($userId, ["EMAIL" => $new_email, "LOGIN" => $new_email]);
		
		$strError .= $user->LAST_ERROR;
		
		if ($resUpdate)
			CEvent::Send("CHANGE_EMAIL_PROMO", ["s1", "s2"], ["USER_ID" => $userId, "EMAIL" => $new_email, "OLD_EMAIL" => $old_email], "N", 99);
		
	}
	else { $strError .= "Пароль введен не верно"; };
}

echo json_encode(['new_email' => $new_email, 'str_error' => $strError]);

?>