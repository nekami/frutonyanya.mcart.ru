<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if ($_POST['g-recaptcha-response'])
{		
	/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
	$Return = getCaptcha($_POST['g-recaptcha-response']);
			
	/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
	if ($Return->success != true && $Return->score <= 0.5)
	{
		echo json_encode(["recaptcha" => "Письма от роботов не принимаются."]);
		die;
	}			
}

if ( !empty($_POST['USER_LOGIN']) && !empty($_POST['USER_PASSWORD']) ) {	
	
	CModule::IncludeModule('main');
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;	

	$LOGIN =    trim($_POST['USER_LOGIN']);	
	$PASSWORD = trim($_POST['USER_PASSWORD']);	
	
	$result = \Bitrix\Main\UserTable::getList(array(
		'select' => array('ID', 'LOGIN', 'UF_CONFIRMATION_EMAIL'),
		'filter' => ['ACTIVE' => 'Y', 'LOGIN' => $LOGIN],
	));

	$idUser = '';	
	$loginUser = '';	

	if ($arUser = $result->fetch()) 
	{
		if ( !$arUser['UF_CONFIRMATION_EMAIL'] )
			{ echo json_encode(['login' => 'Ваш email не подтвержден']); die; }
		
		$idUser = $arUser['ID'];
		$loginUser = $arUser['LOGIN'];
	}
	
	if ( empty($idUser) ) { echo json_encode(['login' => 'E-mail введен не верно']); die; }
	
	// проверка пароля
	$PasswordError = isUserPassword($idUser, $PASSWORD);	
	if ( empty($PasswordError) ) { echo json_encode(['pass' => 'Пароль введен не верно']); die; }
	
	$arAuthResult = $USER->Login($loginUser, $PASSWORD, "Y");
	$APPLICATION->arAuthResult = $arAuthResult;		
	echo json_encode(['success' => $APPLICATION->arAuthResult]);
} 	
?>