function ClearErrorText ()
{
	$(document).on('input', 'input', function () { 	  
		if ($(this).hasClass('errortext'))
		{			
			$(this).removeClass("errortext").next('div.error_text').hide();			
		}		
	 });
	 
	 $(document).on('change', 'select', function () { 	  
		if ($(this).hasClass('errortext'))
		{			
			$(this).removeClass("errortext").next('div.error_text').hide();
		}		
	 });			
}		
	

$(function() 
{	
	$(document).on('click', 'button[data-popup="changeMail"]', function() 
	{	
		$('input[name="old-email"]').val($('a.lc-header__email-link').text());	
	});
	
	// изменение e-mail
	
	$(document).on('click', 'button#changeMailSubmit', function() 
	{	
		event.preventDefault();
		var email     = $('input[name="changeMail"]').val();
		var pass      = $('input[name="profilePassword"]').val();

		if (!email || !pass)
		{
			if (!email)
				$('input[name="changeMail"]').addClass("errortext").next('div.error_text').show();
					
			if (!pass) 
				$('input[name="profilePassword"]').addClass("errortext").next('div.error_text').show();  
		} 
		else 
		{				
			var me = this;
			
			$.ajax({
				url: '/local/templates/promo/ajax/change_mail_submit.php',
				data: $('form#changeMailForm').serialize(),
				method: 'POST',
				type: 'POST',
				dataType: 'json',
				success: function(data)
				{				
					if (data.str_error == "") 
					{				
						$(me).parents('.popup.active').removeClass('active');	
						$('[data-popup="forgotpasswd"] .popup__descr').html('На Ваш старый e-mail отправлено письмо. Перейдите по ссылке из этого письма, чтобы подтвердить новый e-mail.');
						$('[data-popup="forgotpasswd"]').addClass('active');
						
						$(document).on('click', '.js-close-popup', function() 
						{
							$(location).attr('href', '/promo/profile/?logout=yes');
						});
						
					}
					else
					{
						$('input[name="profilePassword"]').addClass("errortext").next('div.error_text').html(data.str_error).show();
					}
				},         
				error:  function(data) {
					console.log('Ошибка ajax-скрипта');
				}
			});
		}
	});	
	
	

}); 