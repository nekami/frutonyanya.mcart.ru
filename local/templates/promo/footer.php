    </div>
    <div class="footer">
      <div class="container footer__container">
        <div class="footer__content"><b>© 2011-2020, АО «Прогресс»</b>
          <p>Публикации, советы и видеоматериалы на сайте <a href="https://frutonyanya.ru/" target="_blank">frutonaynya.ru</a> носят информативный характер. Консультации специалистов портала носят справочный характер и не могут заменить визит к вашему лечащему врачу.</p>
        </div>
        <div class="footer__contact">
          <div class="footer__contact-logo"></div>
          <div class="footer__contact-wrap">
            <div class="footer__contact-icon"></div><a class="footer__contact-phone" href="tel:88002001400">8 800 200 1 400</a>
          </div>
          <div class="footer__contact-text">Бесплатно для <br/>звонков по России</div>
        </div>
        <div class="footer__social">
          <div class="footer__social-left">
            <div class="footer__social-giraffe"></div>
          </div>
          <div class="footer__social-right">
            <div class="footer__social-text">«ФрутоНяня» <br/> в социальных сетях:</div>
            <div class="footer__social-links">
              <div class="social">
                <ul class="social__list">
                  <li class="social__item"><a class="social__link social__link--fb" href="https://www.facebook.com/progressfood">facebook</a></li>
                  <li class="social__item"><a class="social__link social__link--vk" href="https://vk.com/official_frutonyanya">vkontakte</a></li>
                  <li class="social__item"><a class="social__link social__link--inst" href="https://www.instagram.com/frutonyanya_official/">instagram</a></li>
                  <li class="social__item"><a class="social__link social__link--ok" href="https://ok.ru/frutonyanya">ok</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	
	
      <div class="popup">
        <button class="button popup__close js-close-popup" type="button"></button>
        <div class="popup__modal">
          <div class="popup__container">
            <div class="popup__body">
              <div class="popup__content popup__content--mail" data-id="changeMail">
                <div class="popup__header">
                  <div class="popup__title">Изменение адреса электронной почты</div>
                </div>
                <div class="popup__form">				
                  <form class="regForm" action="" method="post" id="changeMailForm">
				    <input type="hidden" name="old-email" value="" />
                    <div class="regForm__row regForm__row--popup">
                      <label class="regForm__label">
                        <div class="regForm__input-name">Новый адрес электронной почты</div>
                        <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
                          <input class="regForm__input" type="email" name="changeMail" required="required" placeholder="Ваш e-mail" id="changeMail"/>
						  <div class="error_text">Поле обязательно для заполнения</div>
                        </div>
                      </label>
                      <label class="regForm__label">
                        <div class="regForm__input-name">Пароль от профиля</div>
                        <div class="regForm__input-wpap"><span class="regForm__input-icon"></span>
                          <input class="regForm__input" type="password" name="profilePassword" required="required" placeholder="Ваш пароль" id="profilePassword"/>
						  <div class="error_text">Поле обязательно для заполнения</div>
                        </div>
                      </label>
                    </div>
                    <label class="regForm__label-checkbox regForm__label-checkbox--popup">
                      <input class="regForm__checkbox" type="checkbox" name="mailing" id="mailing"/><i></i><span class="regForm__checkbox-name">Получать рассылку от АО «Прогресс» на указанный адрес электронной почты</span>
                    </label>
					
					<div class="regForm__row regForm__row--popup" data-name="g-recaptcha-response">
						<input type="hidden" id="g-recaptcha-response_5" name="g-recaptcha-response" value="">				
						<div class="error_text">От роботов письма не принимаются.</div>
				    </div>
					
                    <button class="regForm__submit regForm__submit--full button button--blue" type="submit" id="changeMailSubmit">Отправить</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	  
	  
      <div class="popup" data-popup="forgotpasswd">
        <button class="button popup__close js-close-popup" type="button"></button>
        <div class="popup__modal">
          <div class="popup__container">
            <div class="popup__body">
              <div class="popup__content popup__content--success" data-id="successMail">
                <div class="popup__header">
                  <div class="popup__title">Спасибо!</div>
                </div>
                <div class="popup__form">
                  <div class="popup__descr">На указанный e-mail вам отправлено письмо. Перейдите по ссылке из этого письма, чтобы завершить процедуру смены e-mail</div>
                </div>
                <button class="button button--blue popup__btn js-close-popup" type="button">Хорошо</button>
              </div>
            </div>
          </div>
        </div>
      </div>	  
	
	
    <div class="popup__list">
      <div class="popup">
        <button class="button popup__close js-close-popup" type="button"></button>
        <div class="popup__modal">
          <div class="popup__container">
            <div class="popup__body">
              <div class="popup__content popup__content--auth" data-id="auth">
                <div class="popup__header">
                  <div class="popup__title">Авторизация</div>
                </div>
				
				<?$APPLICATION->IncludeComponent("bitrix:system.auth.form","",Array(
					"REGISTER_URL" => "/promo/registration/",
					"FORGOT_PASSWORD_URL" => "",
					"PROFILE_URL" => "/promo/profile/",
					"SHOW_ERRORS" => "Y" 
				  )
				);?>

              </div>
            </div>
          </div>
        </div>
      </div>
	  	  
      <div class="popup">
        <button class="button popup__close js-close-popup" type="button"></button>
        <div class="popup__modal">
          <div class="popup__container">
            <div class="popup__body">
              <div class="popup__content popup__content--password" data-id="changePassword">
                <div class="popup__header">
                  <div class="popup__title">Забыли пароль?</div>
                </div>
				
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.auth.forgotpasswd",
					"flat",
					Array()
				);?>
                
              </div>
            </div>
          </div>
        </div>
      </div>
	  
    </div>
    
  </body>
</html>