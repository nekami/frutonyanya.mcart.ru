<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8"/>    
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta content="telephone=no" name="format-detection"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>      
    <link rel="icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico"/>
	
	<? CJSCore::Init(["jquery", "ajax"]); ?>
	
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/app.min.js" );?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js" );?>
	<?$APPLICATION->AddHeadScript('https://www.google.com/recaptcha/api.js?render='.SITE_KEY);?>
	
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
  </head>
  
  <body class="page">
  
    <?//$APPLICATION->ShowPanel();?>
	
	<div class="page__wrapper" id="wrapper">
      <div class="header">
        <div class="header__container">
          <div class="header__logo"><a class="header__logo-link js-scroll" href="/promo/"><img class="header__logo-img" src="<?=SITE_TEMPLATE_PATH?>/img/header/logo.png" alt="Логотип Фрутоняня"/></a></div>
          <nav class="header__menu">
            <div class="header__menu-wrap">
              <ul class="header__menu-list">
                <li class="header__menu-item"><a class="header__menu-link js-scroll" href="/promo/#action">О конкурсе</a></li>
                <li class="header__menu-item"><a class="header__menu-link js-scroll" href="/promo/#product">Пюре с фрутокрышками</a></li>
                <li class="header__menu-item"><a class="header__menu-link js-scroll" href="/promo/#play">Идеи</a></li>
                <li class="header__menu-item"><a class="header__menu-link js-scroll" href="/promo/#works">Победители</a></li>
              </ul>
              <div class="header__extra">
                <div class="header__extra-item">
                  <div class="header__extra-btn"><a class="button button--green" href="https://frutonyanya.ru/products/filter/sectionfilter-is-%D0%BC%D1%8F%D0%B3%D0%BA%D0%B0%D1%8F%20%D1%83%D0%BF%D0%B0%D0%BA%D0%BE%D0%B2%D0%BA%D0%B0/apply/" target="_blank"><span>Купить</span></a></div>
                </div>
                <div class="header__extra-item">
                  <? if ($USER->IsAuthorized()) { ?>					
						<!-- Личный кабинет-->
						<a class="header__extra-link" href="/promo/profile/">						
							<span class="header__extra-link__text">личный кабинет</span>
						</a>						
						<a href="?logout=yes" class="header__extra-link__icon"></a>						
					<? } else { ?>						
						<!-- Войти-->
						<div class="header__extra-btn"><a class="button button--blue header__extra-btn js-popup-open" href="#" data-popup="auth"><span>Войти</span></a></div>							
					<? } ?>
                </div>
              </div>
            </div>
          </nav>
          <div class="header__hamburger">
            <button class="button header__hamburger-btn" type="button"><span class="header__hamburger-bar"></span></button>
          </div>
        </div>
      </div>